<?php
/*
Template Name: UTMS Events Calendar
*/

get_header(); ?>

<div class="container" id="logoSection">
			
	<div id="mainLogo" class="row noSlider">
	
		<section class="sevencol">
			<h1 id="pageTitle"><a href="<?php echo tribe_get_events_link(); ?>">Events Calendar</a></h1>
		</section>
		
		<section class="fivecol last">
			<h2 id="utmsasSecondary"><a href="<?php echo site_url(); ?>" title="University of Texas Medical School at Houston" target="_parent"><img src="<?php bloginfo('template_directory'); ?>/img/logo/medschool-logo-large-current.png" alt="University of Texas Medical School at Houston" /></a></h2>
		</section>
		
	</div><!--end row-->
	
</div><!-- ending #logoSection-->

<div class="container" id="mainContent">

	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			
		<article class="post row" id="post-<?php the_ID(); ?>">

			<div class="entry">

				<?php the_content(); ?>

			</div>

		</article>
		
		<?php // comments_template(); ?>

		<?php endwhile; endif; ?>
</div>
		
<?php get_footer(); ?>
