<?php 
/*
Template Name: UTMS UTPro
*/

get_header(); ?>

<div class="container" id="logoSection">
			
	<div id="mainLogo" class="row">
	
		<section class="sevencol">
			<h1 id="pageTitle"><img src="<?php bloginfo('template_directory') ?>/img/utpro/logo-utpro.png" alt="UT★Pro" /><?php //the_title(); ?></h1>
		</section>
		
		<section class="fivecol last">
			<h2 id="utmsasSecondary"><a href="<?php echo site_url(); ?>" title="University of Texas Medical School at Houston" target="_parent"><img src="<?php bloginfo('template_directory'); ?>/img/logo/medschool-logo-large-current.png" alt="University of Texas Medical School at Houston" /></a></h2>
		</section>
		
	</div><!--end row-->
	
</div><!-- ending #logoSection-->

<div class="container" id="mainContent">
	
	<div class="row">
	
		<section class="threecol">
			<div id="utpro-section">
				<img src="<?php bloginfo('template_directory') ?>/img/utpro/utpro-card.png" alt="UT★Pro Card" class="utprobgimg" />
			</div><!--ending utpro-->
			<?php include('includes/left-sidebar.php'); ?>
		</section>
		
		<section id="mainArticle" class="sixcol">
			
			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

				<article <?php post_class() ?> id="post-<?php the_ID(); ?>">
				
					<div id="utproentry" class="entry">
						<?php the_content(); ?>
					</div>
		
				</article>
		
			<?php endwhile; ?>
				
			<?php endif ; ?>
						
		</section><!-- ending #contentDiv -->
		
		<aside class="threecol last">		
			<?php get_sidebar(); ?>
			<?php include (TEMPLATEPATH . '/includes/righticon-buttons.php'); ?>
		</aside>
		
	
	</div>
	
</div><!--ending #mainContent-->

<?php get_footer(); ?>
