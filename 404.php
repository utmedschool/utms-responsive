<?php get_header(); ?>

<div class="container" id="logoSection">
			
	<div id="mainLogo" class="row noSlider">
	
		<section class="sevencol">
			<h1 id="pageTitle">Error !</h1>
		</section>
		
		<section class="fivecol last">
			<h2 id="utmsasSecondary"><a href="<?php echo site_url(); ?>" title="University of Texas Medical School at Houston" target="_parent"><img src="<?php bloginfo('template_directory'); ?>/img/logo/medschool-logo-large-current.png" alt="University of Texas Medical School at Houston" /></a></h2>
		</section>
		
	</div><!--end row-->
	
</div><!-- ending #logoSection-->

<?php include('includes/mission-nav.php'); ?>

<div class="container" id="mainContent">
	
	<div class="row">
	
		<section id="mainArticle" class="ninecol">
	
			<h1><?php _e('Error 404 - Page Not Found'); ?></h1>
			
			<p>The page you were looking for has been moved, deleted or does not exist.</p>
			
			<p>Please report this error to the University Webmaster.</p>
			
			<p>Try returning to the <a href="<?php echo site_url(); ?>">Medical School University home page</a> to find the page for which you were looking.</p>
			
			<p>Please view the list of links below.</p>
			
			<div class="fourcol">
			<h2>Pages</h2>
			<ul>
			<?php wp_list_pages('title_li='); ?>
			</ul>
			</div>
			
			<div class="eightcol last">
			<h2>News Articles</h2>
			<ul>
			<?php $recent = query_posts('category_name=news&showposts=-1'); while (have_posts()) : the_post(); ?>
			<li><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></li>
			<?php endwhile; ?>
			</ul>
			</div>
				
		</section><!-- ending #mainArticle -->
		
		
		<section class="threecol last">
			
			<?php get_sidebar(); ?>
			
			<?php include (TEMPLATEPATH . '/includes/righticon-buttons.php'); ?>
			
		</section>
	</div>
	
	<div class="row">
	
	<div class="twelvecol last">
		
		<?php wp_list_bookmarks('title_li=&category_before=&category_after='); ?>
		
		</div>
	</div>
	
	
</div><!--ending #mainContent-->

<?php get_footer(); ?>	