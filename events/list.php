<?php
/**
* The TEC template for a list of events. This includes the Past Events and Upcoming Events views 
* as well as those same views filtered to a specific category.
*
* You can customize this view by putting a replacement file of the same name (list.php) in the events/ directory of your theme.
*/

// Don't load directly
if ( !defined('ABSPATH') ) { die('-1'); }

?>
<div class="container">

<div class="row">

<div class="twelvecol last">

<span class='tribe-events-calendar-buttons'> 
	<a class='tribe-events-button-on' href='<?php echo tribe_get_gridview_link(); ?>'><?php _e('Calendar', 'tribe-events-calendar')?></a>
</span>

<ul id="tribe-events-loop">
	
	<?php if (have_posts()) : ?>
	<?php $hasPosts = true; $first = true; ?>
	<?php while ( have_posts() ) : the_post(); ?>
		<?php global $more; $more = false; ?>
		<li id="post-<?php the_ID() ?>" itemscope itemtype="http://schema.org/Event">
			<!--<?php while ( tribe_is_new_event_day() && !tribe_is_day() ) : ?>
				<h4 class="event-day"><?php echo tribe_get_start_date( null, false ); ?></h4>
			<?php endwhile; ?>-->
			
			<span class="entry-series"><?php echo tribe_get_event_meta($post->ID, $meta = 'event-series'); ?>: </span>
			<span class="entry-organizer"><?php echo tribe_get_organizer(); ?> presents</span>
			<?php the_title('<span class="entry-title" itemprop="name"><a href="' . tribe_get_event_link() . '" title="' . the_title_attribute('echo=0') . '" rel="bookmark">', '</a></span>'); ?>
				<?php if (has_excerpt ()): ?>
					<?php the_excerpt(); ?>
				<?php else: ?>
					<?php the_content(); ?>
				<?php endif; ?>
			<?php if (tribe_is_multiday() || !tribe_get_all_day()): ?>
					<span class="entry-time"><?php _e('Start:', 'tribe-events-calendar') ?>
					<?php echo tribe_get_start_date(); ?></span>
					<span class="entry-time"><?php _e('End:', 'tribe-events-calendar') ?>
					<?php echo tribe_get_end_date(); ?></span>
					<?php else: ?>
					<span class="entry-date"><?php _e('Date:', 'tribe-events-calendar') ?>
					<?php echo tribe_get_start_date(); ?></span>
					<?php endif; ?>
					<?php
						$venue = tribe_get_venue();
						if ( !empty( $venue ) ) :
					?>
					<span class="entry-venue"><?php _e('Venue:', 'tribe-events-calendar') ?>
					
							<? if( class_exists( 'TribeEventsPro' ) ): ?>
								<?php tribe_get_venue_link( get_the_ID(), class_exists( 'TribeEventsPro' ) ); ?>
							<? else: ?>
								<?php echo tribe_get_venue( get_the_ID() ) ?></span>
							<? endif; ?>
						
					<?php endif; ?>
					<?php
						$phone = tribe_get_phone();
						if ( !empty( $phone ) ) :
					?>
					<?php _e('Phone:', 'tribe-events-calendar') ?>
					<?php echo $phone; ?>
					
					<?php endif; ?>
					
					<!--
					<?php if (tribe_address_exists( get_the_ID() ) ) : ?>
					
					<?php _e('Address:', 'tribe-events-calendar'); ?><br />
						<?php if( get_post_meta( get_the_ID(), '_EventShowMapLink', true ) == 'true' ) : ?>
							<a class="gmap" itemprop="maps" href="<?php echo tribe_get_map_link(); ?>" title="Click to view a Google Map" target="_blank"><?php _e('Google Map', 'tribe-events-calendar' ); ?></a>
						<?php endif; ?>
						<?php echo tribe_get_full_address( get_the_ID() ); ?>
						
					<?php endif; ?>
					<?php
						$cost = tribe_get_cost();
						if ( !empty( $cost ) ) :
					?>
					
					<?php _e('Cost:', 'tribe-events-calendar') ?>
					<?php echo $cost; ?>
					
					<?php endif; ?>-->

		</li> <!-- End post -->
	<?php endwhile;// posts ?>
	<?php else :?>
		<?php 
			$tribe_ecp = TribeEvents::instance();
			if ( is_tax( $tribe_ecp->get_event_taxonomy() ) ) {
				$cat = get_term_by( 'slug', get_query_var('term'), $tribe_ecp->get_event_taxonomy() );
				if( tribe_is_upcoming() ) {
					$is_cat_message = sprintf(__(' listed under %s. Check out past events for this category or view the full calendar.','tribe-events-calendar'),$cat->name);
				} else if( tribe_is_past() ) {
					$is_cat_message = sprintf(__(' listed under %s. Check out upcoming events for this category or view the full calendar.','tribe-events-calendar'),$cat->name);
				}
			}
		?>
		<?php if(tribe_is_day()): ?>
			<?php printf( __('No events scheduled for <strong>%s</strong>. Please try another day.', 'tribe-events-calendar'), date_i18n('F d, Y', strtotime(get_query_var('eventDate')))); ?>
		<?php endif; ?>

		<?php if(tribe_is_upcoming()){ ?>
			<?php _e('No upcoming events', 'tribe-events-calendar');
			echo !empty($is_cat_message) ? $is_cat_message : ".";?>

		<?php }elseif(tribe_is_past()){ ?>
			<?php _e('No previous events' , 'tribe-events-calendar');
			echo !empty($is_cat_message) ? $is_cat_message : ".";?>
		<?php } ?>
		
	<?php endif; ?>


	</ul><!-- #tribe-events-loop -->
	
	</div>
	
	<div class="twelvecol last">
	
	<div id="tribe-events-nav-below" class="tribe-events-nav clearfix">

		<div class="tribe-events-nav-previous"><?php 
		// Display Previous Page Navigation
		if( tribe_is_upcoming() && get_previous_posts_link() ) : ?>
			<?php previous_posts_link( '<span>'.__('&laquo; Previous Events', 'tribe-events-calendar').'</span>' ); ?>
		<?php elseif( tribe_is_upcoming() && !get_previous_posts_link( ) ) : ?>
			<a href='<?php echo tribe_get_past_link(); ?>'><span><?php _e('&laquo; Previous Events', 'tribe-events-calendar' ); ?></span></a>
		<?php elseif( tribe_is_past() && get_next_posts_link( ) ) : ?>
			<?php next_posts_link( '<span>'.__('&laquo; Previous Events', 'tribe-events-calendar').'</span>' ); ?>
		<?php endif; ?>
		</div>

		<div class="tribe-events-nav-next"><?php
		// Display Next Page Navigation
		if( tribe_is_upcoming() && get_next_posts_link( ) ) : ?>
			<?php next_posts_link( '<span>'.__('Next Events &raquo;', 'tribe-events-calendar').'</span>' ); ?>
		<?php elseif( tribe_is_past() && get_previous_posts_link( ) ) : ?>
			<?php previous_posts_link( '<span>'.__('Next Events &raquo;', 'tribe-events-calendar').'</span>' ); // a little confusing but in 'past view' to see newer events you want the previous page ?>
		<?php elseif( tribe_is_past() && !get_previous_posts_link( ) ) : ?>
			<a href='<?php echo tribe_get_upcoming_link(); ?>'><span><?php _e('Next Events &raquo;', 'tribe-events-calendar'); ?></span></a>
		<?php endif; ?>
		</div>

	</div>
	
	</div>
	
</div>
</div>
</div>