<?php
/**
 * Grid view template.  This file loads the TEC month view, specifically the 
 * month view navigation.  The actual rendering if the calendar happens in the 
 * table.php template.
 *
 * You can customize this view by putting a replacement file of the same name (gridview.php) in the events/ directory of your theme.
 */

// Don't load directly
if ( !defined('ABSPATH') ) { die('-1'); }
$tribe_ecp = TribeEvents::instance();
?>	

<div id="calendarPage" class="row">

<div id="eventsSection" class="threecol">

		<article id="submissionCriteria" class="white45">
		
		<h2>Events Submission Criteria</h2>
		
		<p>Campus Web Calendar events must be sponsored by an officially recognized Medical School department, office, student group, or Medical School organization.</p>
		<p>Campus Calendar editors reserve the right to delete and/or edit event listings. For all inquiries email us at <a href="mailto:ms.webcalendar@uth.tmc.edu" title="email us">ms.webcalendar@uth.tmc.edu</a></p>
		<p>To fill out a request for an event to be posted on the calendar:</p>
		<ul id="event-calendar-links">
			<li class="css3button"><a href="https://med.uth.edu/create-an-event/" title="Submit an Event">Submit an event</a></li>
		</ul>
		
		</article>
		
		<article id="icalHolder" class="white45" style="margin-bottom:15px;">
			<?php if( function_exists( 'tribe_get_ical_link' ) ): ?>
			<span class="css3button">
	         <a title="<?php esc_attr_e('iCal Import', 'tribe-events-calendar') ?>" class="css3button" href="<?php echo tribe_get_ical_link(); ?>"><?php _e('iCal Import', 'tribe-events-calendar') ?></a>
	         </span>
	      <?php endif; ?>
		</article>
		
		<nav>
		<?php 
		wp_nav_menu(array(
			'menu' => 'links-and-resources', 
			'container' => 'ul',
			'menu_id' => 'linksResources', 
			'menu_class' => 'menu widget'
		)); ?> 
		</nav>
		
		<h2>resources for</h2>
		<nav>
		<?php 
		wp_nav_menu(array(
			'menu' => 'links for', 
			'container' => 'ul',
			'menu_id' => 'linksFor', 
			'menu_class' => 'menu widget'
		)); ?> 
		</nav>

		
</div>

<div class="ninecol last">

	<div id="tribe-events-content" class="grid">
      <!-- This title is here for ajax loading - do not remove if you wish to use ajax switching between month views -->
      <title><?php wp_title() ?></title>
		<div id='tribe-events-calendar-header' class="clearfix">
			<span class='tribe-events-month-nav'>
			
			<span class="currentMonth">
				<!--<?php echo date('M. Y'); ?>-->
			</span>
			
				<span class='tribe-events-prev-month'>
					<a href='<?php echo tribe_get_previous_month_link(); ?>'>
					&#x2190; <?php echo tribe_get_previous_month_text(); ?>
					</a>
				</span>

				<?php tribe_month_year_dropdowns( "tribe-events-" ); ?>
	
				<span class='tribe-events-next-month'>
					<a href='<?php echo tribe_get_next_month_link(); ?>'>				
					<?php echo tribe_get_next_month_text(); ?> &#x2192; 
					</a>
               <img src="<?php echo esc_url( admin_url( 'images/wpspin_light.gif' ) ); ?>" class="ajax-loading" id="ajax-loading" alt="" style='display: none'/>
				</span>
			</span>
			
		</div><!-- tribe-events-calendar-header -->
		<?php tribe_calendar_grid(); // See the views/table.php template for customization ?>		
	</div>

	<section id="upcomingThree" class="twelvecol last">
		
		<h2>upcoming events</h2>
		
		<ul class="eventListing sixcol">
		<?php 
		
		global $post;
		$all_events = tribe_get_events(array(
					'eventDisplay'=>'upcoming',
					'posts_per_page'=>8
				));
		
		foreach($all_events as $post) { setup_postdata($post); ?>
		<li class="twelvecol last">
		<a href="<?php the_permalink(); ?>" id="post-<?php the_ID(); ?>">
		<div class="eventDate">
			<span class="month"><?php echo tribe_get_start_date($post->ID, false, 'M'); ?></span>
			<span class="day"><?php echo tribe_get_start_date($post->ID, false, 'j'); ?></span>
		</div>
		<div class="eventDetails">
			 <?php 
		$themeta = get_post_meta($post->ID, 'event-series', TRUE);
		 if ($themeta != '') { ?>
			<span class="event-series"><?php echo tribe_get_event_meta($post->ID, $meta = 'event-series'); ?>: </span>
		<?php } ?>
				<?php if (tribe_get_organizer()) { ?>
				<span class="event-organizer"><?php echo tribe_get_organizer(); ?> presents</span>
				<?php } ?>
		        <span class="event-title"><?php the_title(); ?></span>
		        	<?php if(tribe_get_venue()) : ?>
					<span class="event-venue">
						<?php echo tribe_get_venue(get_the_ID()); ?>
					, </span>
					<?php endif; ?>
				<span class="event-date"><?php echo tribe_get_start_date($post->ID, true, ' '); ?> <?php echo tribe_get_end_date( $post->ID, true, '–' );?><br /><?php echo tribe_get_start_date($post->ID, false, 'M j, Y'); ?></span>
		        <?php
		        $excerpt = strip_tags(get_the_content(), '<p>');
				//$excerpt = preg_replace("/\<a([^>]*)\>([^<]*)\<\/a\>/i", "$2", $excerpt);
				echo '<span class="event-content">'.$excerpt. '</span>';
		        ?>
		        
		 </div></a>
		    </li>
		<?php } //endforeach ?>
		<?php wp_reset_query(); ?>
		</ul>
		
		<ul class="eventListing sixcol last">
		<?php 
		
		global $post;
		$all_events = tribe_get_events(array(
					'eventDisplay'=>'upcoming',
					'posts_per_page'=>7,
					'offset' => 8
				));
		
		foreach($all_events as $post) { setup_postdata($post); ?>
		<li class="twelvecol last">
		<a href="<?php the_permalink(); ?>" id="post-<?php the_ID(); ?>">
		<div class="eventDate">
			<span class="month"><?php echo tribe_get_start_date($post->ID, false, 'M'); ?></span>
			<span class="day"><?php echo tribe_get_start_date($post->ID, false, 'j'); ?></span>
		</div>
		<div class="eventDetails">
			 <?php 
		 $themeta = get_post_meta($post->ID, 'event-series', TRUE);
		 if ($themeta != '') { ?>
			<span class="event-series"><?php echo tribe_get_event_meta($post->ID, $meta = 'event-series'); ?>: </span>
		<?php } ?>
				<?php if (tribe_get_organizer()) { ?>
				<span class="event-organizer"><?php echo tribe_get_organizer(); ?> presents</span>
				<?php } ?>
		        <span class="event-title"><?php the_title(); ?></span>
		        	<?php if(tribe_get_venue()) : ?>
					<span class="event-venue">
						<?php echo tribe_get_venue(get_the_ID()); ?>
					, </span>
					<?php endif; ?>
				<span class="event-date"><?php echo tribe_get_start_date($post->ID, true, ' '); ?> <?php echo tribe_get_end_date( $post->ID, true, '–' );?><br /><?php echo tribe_get_start_date($post->ID, false, 'M j, Y'); ?></span>
		         <?php
		        $excerpt = strip_tags(get_the_content(), '<p>');
				//$excerpt = preg_replace("/\<a([^>]*)\>([^<]*)\<\/a\>/i", "$2", $excerpt);
				echo '<span class="event-content">'.$excerpt. '</span>';
		        ?>
		 </div></a>
		    </li>
		<?php } //endforeach ?>
		<?php wp_reset_query(); ?>
		</ul>
		
		
		</section>

</div>

</div>
