<?php
/*---------------------------------------
*  Post GF Form data to Events Calendar Pro
*  
*  Create front-end submission forms by 
*  formatting GF Form data for Events
*  Calendar Pro API
*
*  For more info and GF form boilerplate visit:
*  http://anthonydispezio.com/blog/gf-ecp-frontend-submission/ ‎
----------------------------------------*/

//  Add the action hook to format the GF form data into ECP event meta
//  The GF form id is added to the "gform_pre_submission" hook so it only fires when that form is submitted
add_action("gform_pre_submission_38", "format_ecp_event_meta_from_gravity");

function format_ecp_event_meta_from_gravity(){

	/*  VARIABLES - The following variables should correspond to their respective GF form elements IDs */
	
	// All day event
	$eventAllDay = 35;

	// Start and end dates (and times if the event is not all day)
	$startDateFormId = 18;
	$startTimeFormId = 19;
	$endDateFormId = 38;
	$endTimeFormId = 21;

	// Event recurrence type
	//$recType = 8;

	// Recurrence ends "On" a speceific date or "After" a certain number of occurrences
	//$recEndType = 9;

	// End date for event recurrence (if "On" is selected)
	//$recEnd = 10;

	// A different "After" multiplier exists for each possible recurrence type (if "After" is selected)
	/*$recEndCounts = array(
		'Every Day' => 11,
		'Every Week' => 12,
		'Every Month' => 13,
		'Every Year' => 14,
		);
	*/

	// Venue details
	$venueName = 39;
	$venueAddress = 40;
	$venueCity = 41;
	$venueCountry = 45;

	// for neither US or Canada, use province text field
	//$venueProvince = 22;

	// for US, use state dropdown (two letter values have been added to match ECP meta)
	$venueState = 42;

	// for Canada, use province/territory dropdown
	//$venueCaProvince = 24;

	$venueZip = 43;
	//$venuePhone = 36;

	// Google Maps
	//$showGoogleMapLink = 26;
	//$showGoogleMap = 27;

	//Organizer details
	$organizerName = 44;
	//$organizerPhone = 30;
	//$organizerWebsite = 31;
	//$organizerEmail = 32;

	/*  DATE & TIME FORMATTING - Format the date and time from GF to match ECP meta */
	
	// break the dates into arrays
	$startDate = date_parse($_POST['input_'. $startDateFormId]); 
	$endDate = date_parse($_POST['input_'. $endDateFormId]);

	// sql format the result
	$startDateString = $startDate['year'] . '-' . str_pad($startDate['month'], 2, "0", STR_PAD_LEFT) . '-' . str_pad($startDate['day'], 2, "0", STR_PAD_LEFT);
	$endDateString = $endDate['year'] . '-' . str_pad($endDate['month'], 2, "0", STR_PAD_LEFT) . '-' . str_pad($endDate['day'], 2, "0", STR_PAD_LEFT);

	// get the start/end times
	$startTime = $_POST['input_'. $startTimeFormId];
	$endTime = $_POST['input_'. $endTimeFormId];

	/* SET ECP FORM VALUES - Set the ECP form values to match their respective GF fields */

	$_POST['EventAllDay'] = $_POST['input_'. $eventAllDay];	

	$_POST['EventStartDate'] = $startDateString;
	$_POST['EventStartHour'] = str_pad($startTime[0], 2, "0", STR_PAD_LEFT);
	$_POST['EventStartMinute'] = str_pad($startTime[1], 2, "0", STR_PAD_LEFT);
	$_POST['EventStartMeridian'] = $startTime[2];

	$_POST['EventEndDate'] = $endDateString;
	$_POST['EventEndHour'] = str_pad($endTime[0], 2, "0", STR_PAD_LEFT);
	$_POST['EventEndMinute'] = str_pad($endTime[1], 2, "0", STR_PAD_LEFT);
	$_POST['EventEndMeridian'] = $endTime[2];

	

	// Check for the existence of the submitted venue and organization by title
	$savedVenue = get_page_by_title($_POST['input_'. $venueName], 'OBJECT', 'tribe_venue');
	$savedOrganizer = get_page_by_title($_POST['input_'. $organizerName], 'OBJECT', 'tribe_organizer');

	// If the venue already exists, pass along the exising venue ID
	if (isset($savedVenue)){
		$_POST['venue']['VenueID'] = $savedVenue->ID;
	// If the venue doesn't exist, pass the venue meta needed to create a new venue
	} else {
		// Required for venue info to be stored
		$_POST['EventVenue'] = $_POST['input_'. $venueName];
		$_POST['post_title'] = $_POST['input_'. $venueName];

		// Pass remaining venue meta
		$_POST['venue']['Venue'] = $_POST['input_'. $venueName];
		$_POST['venue']['Address'] = $_POST['input_'. $venueAddress];
		$_POST['venue']['City'] = $_POST['input_'. $venueCity];
		$_POST['venue']['Country'] = $_POST['input_'. $venueCountry];
		// Ensure that the correct state or province field is populated
		switch($_POST['input_'. $venueCountry]) {
			case 'United States':
				$_POST['venue']['State'] = $_POST['input_'. $venueState];
				break;
		}
		$_POST['venue']['Zip'] = $_POST['input_'. $venueZip];
		$_POST['venue']['Phone'] = $_POST['input_'. $venuePhone];
	}

	// If the organizer already exists, pass along the exising organizer ID
	if (isset($savedOrganizer)){
		$_POST['organizer']['OrganizerID'] = $savedOrganizer->ID;
	} else {
		// If the organizer doesn't exist, pass the organizer meta needed to create a new organizer
		$_POST['organizer']['Organizer'] = $_POST['input_'. $organizerName];

	}
}


// Store the new form values as ECP metadata when saving
add_action('save_post', 'save_ecp_event_meta_from_gravity', 11, 2);

function save_ecp_event_meta_from_gravity($postId, $post) {
	if( class_exists('TribeEvents') ) {

	// only continue if it's an event post
	if ( $post->post_type != TribeEvents::POSTTYPE || defined('DOING_AJAX') ) {
		return;
	}

	// don't do anything on autosave or auto-draft either or massupdates
	//if ( wp_is_post_autosave( $postId ) || isset($_GET['bulk_edit']) )
	if ( wp_is_post_autosave( $postId ) || $post->post_status == 'auto-draft' || isset($_GET['bulk_edit']) || $_REQUEST['action'] == 'inline-save' )
	{
		return;
	}

	if( class_exists('TribeEventsAPI') ) {
		$_POST['Organizer'] = stripslashes_deep($_POST['organizer']);
		$_POST['Venue'] = stripslashes_deep($_POST['venue']);

		if( !empty($_POST['Venue']['VenueID']) )
		$_POST['Venue'] = array('VenueID' => $_POST['Venue']['VenueID']);

		if( !empty($_POST['Organizer']['OrganizerID']) )
		$_POST['Organizer'] = array('OrganizerID' => $_POST['Organizer']['OrganizerID']);

		TribeEventsAPI::saveEventMeta($postId, $_POST, $post);
		}
	}
} 

	add_action( 'gform_after_submission_38', 'set_timestamp', 10, 2 );
		function set_timestamp( $entry, $form ) {
			$start_date = get_post_meta( $entry['post_id'], 'EventStartDate', true );
			$finish_date = get_post_meta( $entry['post_id'], 'EventEndDate', true );
		if( $start_date ) {
			$timestamp = strtotime( $start_date );
			update_post_meta( $entry['post_id'], 'EventStartDate', $timestamp );
		}
		if( $finish_date ) {
			$timestamp = strtotime( $finish_date );
			update_post_meta( $entry['post_id'], 'EventEndDate', $timestamp );
		}
	}
	
?>