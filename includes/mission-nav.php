<div class="container">
	<div id="missionRow" class="row">
		<ul id="missionNav" class="twelvecol last">
			<li<?php if (is_page('education')) echo ' class="active"'; ?>><a href="<?php echo get_permalink(3603); ?>" title="Education">education</a></li>
			<li<?php if (is_page('patientcare')) echo ' class="active"'; ?>><a href="<?php echo get_permalink(3605); ?>" title="Patient Care">patient care</a></li>
			<li<?php if (is_page('research')) echo ' class="active"'; ?>><a href="<?php echo get_permalink(3608); ?>" title="Research">research</a></li>
			<li<?php if (is_page('departments')) echo ' class="active"'; ?>><a href="<?php echo get_permalink(3610); ?>" title="Departments">departments</a></li>
		</div>
	</div>
</div>