<?php

function register_shortcodes(){
   add_shortcode('slideshow', 'slideshow_function');
}

function slideshow_function() {
$post_id = get_the_ID();

$images = get_children( array( 'post_parent' => $post_id, 'post_type' => 'attachment', 'post_mime_type' => 'image') );

$total_images = count($images);

if($total_images <= 3) {
  $itemWidth = 615 / $total_images;
} else {
  $itemWidth = 150;
}

$return_string .= '<link href="' . get_template_directory_uri() . '/js/FlexSlider2/flexslider.css" rel="stylesheet" />';
$return_string .= '<script type="text/javascript" src="' . get_template_directory_uri() . '/js/FlexSlider2/jquery.flexslider.js"></script>';
$return_string .= '<script type="text/javascript">' . "\n";
$return_string .= 'jQuery(window).load(function() {' . "\n";
$return_string .= '  jQuery("#carousel").flexslider({' . "\n";
$return_string .= '    animation: "slide",' . "\n";
$return_string .= '    controlNav: false,' . "\n";
$return_string .= '    slideshow: false,' . "\n";
$return_string .= '    itemWidth: ' . $itemWidth . ',' . "\n";
$return_string .= '    asNavFor: "#slider"' . "\n";
$return_string .= '  });' . "\n";
$return_string .= '   ' . "\n";
$return_string .= '  jQuery("#slider").flexslider({' . "\n";
$return_string .= '    animation: "slide",' . "\n";
$return_string .= '    controlNav: false,' . "\n";
$return_string .= '    animationLoop: false,' . "\n";
$return_string .= '    itemWidth: 615,' . "\n";
$return_string .= '    slideshow: false,' . "\n";
$return_string .= '    sync: "#carousel"' . "\n";
$return_string .= '  });' . "\n";
$return_string .= '});' . "\n";

$return_string .= '</script>';

$return_string .= '<style>' . "\n";
$return_string .= '.flexslider .slides img {border: none;}' . "\n";
$return_string .= '#slider, #carousel { max-width: 615px;}' . "\n";
$return_string .= '#mainArticle #slider ul, #mainArticle #carousel ul {margin-left:0px; padding-bottom:0px; overflow: hidden;}' . "\n";
$return_string .= '#mainArticle ul li { list-style-type; none; margin-left: 0px; margin-bottom: 0px;}' ."\n";
$return_string .= '#mainArticle ul.flex-direction-nav li a {padding: 0px 0px; background: url(' . get_template_directory_uri() . '/js/FlexSlider2/images/bg_direction_nav.png) no-repeat 0 0; border: none;}' . "\n";
$return_string .= '#mainArticle ul.flex-direction-nav li a.flex-next {background-position: 100% 0; right: -36px;}' . "\n";
$return_string .= '#mainArticle ul.flex-direction-nav li a.flex-prev {left: -36px;}' . "\n";
$return_string .= '#mainArticle ul.flex-direction-nav li a.flex-next {right: 5px;}' . "\n";
$return_string .= '#mainArticle ul.flex-direction-nav li a.flex-prev {left: 5px;}' . "\n";
$return_string .= '#mainArticle p.flex-caption-single {color: white; width:96%; }' . "\n";
$return_string .= '#mainArticle #carousel ul.slides li {margin-left: 2px; cursor: pointer;}' . "\n";
$return_string .= '#mainArticle #carousel ul.slides li:first-child {margin-left: 0px;}' . "\n";
$return_string .= '.rwd_show {display: none;}' . "\n";
$return_string .= '@media only screen and (min-device-width : 320px) and (max-device-width : 480px) {' . "\n";
$return_string .= '.rwd_show {display: block;}' . "\n";
$return_string .= '.rwd_hide {display: none;}' . "\n";
$return_string .= '#mainArticle p.flex-caption-single {display:none;}' . "\n";
$return_string .= '}' . "\n";
$return_string .= '</style>';

$return_string .= '<div id="slider" class="flexslider ">';
$return_string .= '<ul class="slides">';
        if ( $images ) { 
                foreach ( $images as $attachment_id => $attachment ) {
                    if ($attachment->post_excerpt != '') {
                      $return_string .= '<li>' . wp_get_attachment_image( $attachment->ID, 'full' ) .' <p class="flex-caption-single">' . $attachment->post_excerpt . '</p></li>';
                    } else {
                      $return_string .= '<li>' . wp_get_attachment_image( $attachment->ID, 'full' ) .'</li>';
                    }
                
                }
        }
$return_string .= '</ul>';
$return_string .= '</div>';
$return_string .= '<p class="rwd_show" style="text-align: center; font-style: italic;"><em>Swipe to see more photos.</em></p>';
$return_string .= '<div id="carousel" class="flexslider rwd_hide">';
$return_string .= '<ul class="slides">';
    if ( $images ) { 
                foreach ( $images as $attachment_id => $attachment ) {
                $return_string .= '<li>' . wp_get_attachment_image( $attachment->ID, 'thumb' ) .' </li>';
                }
        }
$return_string .= '</ul>';
$return_string .= '</div>';

return $return_string;
}

add_action( 'init', 'register_shortcodes');
?>


