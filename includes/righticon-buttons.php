<ul id="buttonLinks">
	<?php if (!is_home()) { ?><li class="css3button"><a href="<?php echo get_permalink(2994); ?>"><img src="<?php bloginfo('template_directory'); ?>/img/icons/calendar.png" alt="calendar" class="calendarinButton" /><span class="calendarTxt">Submit an event to <br />the calendar</span></a></li> <?php } ?>
	
	<li class="css3button"><a href="https://med.uth.edu/msit/" target="_blank" title="MSIT"><img src="<?php bloginfo('template_directory'); ?>/img/icons/computer-withscreen.png" alt="msit" class="computerinButton" /><span class="msitTxt">IT Problems?<br /><small>Click to connect <br />with MSIT</small></span></a></li>
	
	<li class="css3button"><a href="http://uth.tmc.edu/med/dna/" target="_blank" title="DNA DailyNewsAccess Monitor"><img src="<?php bloginfo('template_directory'); ?>/img/icons/dna.png" alt="dna" class="dnainButton" /><span class="dnaTxt">Submit content to the TV screens</span></a></li>
	
	<li class="css3button"><a href="<?php echo get_permalink(2987); ?>"><img src="<?php bloginfo('template_directory'); ?>/img/icons/envelope-angle.png" alt="envelope" class="envelopeinButton" /><span class="feedback">Have feedback or questions about the website? </span><span class="feedbackline2">Can't find a specific page or information? <small>Send us an email</small></span></a></li>
	
</ul>