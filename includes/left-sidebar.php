	<?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('Left Sidebar Widgets')) : else : ?>
    
        <!-- All this stuff in here only shows up if you DON'T have any widgets active in this zone -->
	
	<?php endif; ?>
	
	<?php if (is_category()) : ?>
	
		<?php dynamic_sidebar( 'category-left-widgets' ); ?>
	
	<?php endif; ?>