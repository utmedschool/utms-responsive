<?php

/* ========================================= */
/* CPT for Feature Image                      */
/* ========================================= */

$featureimage = register_cuztom_post_type(
	'featureimage',
		array(
		    'has_archive' => false,
		    'supports' => array ('title', 'thumbnail', 'excerpt'),
		    'rewrite' => array (
		                        'slug' => 'featureimage',
		                        'with_front' => false
								)
        ),
	   array(
	      'name'      => 'Feature Image',
	      'menu_name' => 'Feature Image',
	      'all_items' => 'All Feature Images',
	      'add_new'   => 'Add New Feature Image',
          'add_new_item' => 'Add New Feature Image'
	   ));
	  
$featureimage->add_taxonomy( 'Feature Category' );

	
$featureimage->add_meta_box(
	'info',
  	'Feature Image Information',
      array(
      	array(
              'name'          => 'photographer',
              'label'         => 'Enter the name of Photographer',
              'description'   => 'Terence T. Tang or Dwight C. Andrews',
              'type'          => 'text'
          ),
          array(
              'name'          => 'link_url',
              'label'         => 'Link to External Post',
              'description'   => 'Use this field if you want to link to and external source',
              'type'          => 'text'
          ),
          array(
              'name'          => 'page_select',
    	        'label'         => 'Page Select',
    	        'description'   => 'Use this field if you want to link to an internal page',
    	        'type'          => 'post_select',
    	        'args'          => array(
    	            'post_type' => 'page',
				            'posts_per_page'	=> -1,
				            'show_option_none' => 'None'
	        )),
          array(
    	        'name'          => 'post_select',
    	        'label'         => 'Post Select',
    	        'description'   => 'Use this field if you want to link to an internal news article',
    	        'type'          => 'post_select',
    	        'args'          => array(
    	           'post_type' => 'post',
		            'show_option_none' => 'None'
	        ))
          
	    )
  );