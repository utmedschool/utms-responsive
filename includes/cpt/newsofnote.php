<?php

/* ========================================= */
/* CPT for News	of Note                      */
/* ========================================= */

$newsofnote = register_cuztom_post_type(
	'newsofnote',
		array(
		    'has_archive' => true,
		    'supports' => array ('title', 'revisions'),
		    'rewrite' => array (
		                        'slug' => 'newsofnote',
		                        'with_front' => false
								)
        ),
	   array(
	      'name'      => 'News of Note',
	      'menu_name' => 'News of Note',
	      'all_items' => 'All News of Notes',
	      'add_new'   => _x( 'Add New', 'News of Note' ),
	   ));

	
$newsofnote->add_meta_box(
	'details',
    'Details',
    array(
        'bundle',
        array(
            array(
                'name'          => 'title',
                'label'         => 'Title',
                'description'		=> 'Enter the title of the news item',
                'type'          => 'text'
            ),
            array(
                'name'          => 'excerpt',
                'label'         => 'Excerpt',
                'description'		=> 'Enter short description',
                'type'          => 'textarea'
            ),
            array(
                'name'          => 'url',
                'label'         => 'URL',
                'description'		=> 'Copy and paste the full URL of the item including the http:// or https://',
                'type'          => 'text'
            )
        )
    )
);