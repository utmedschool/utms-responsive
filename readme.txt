----------------------------------------------------------------------------
                        November 15, 2013 - suBi
----------------------------------------------------------------------------

- Changed Featured images to only 4 in homepage to control load time
- Removed Tweets from homepage to reduce load time
- Added news of note button on the homepage
- Changed link for MSIT

----------------------------------------------------------------------------
                        September 19, 2013 - suBi
----------------------------------------------------------------------------

-Upgraded CPT Helper class to 2.8.2
-Added Featureimage CPT so that everyone can post their own images


----------------------------------------------------------------------------
                        September 20, 2013 - suBi
----------------------------------------------------------------------------
- In the morning, rolled back to 2.6.2 for issues with helper class 
and News of Note
- After a brief conversation with @Gizburdt, updated the News of Note single
template and helper class to 2.8.4 with correct markup and update in the
helper class wiki about using the bundles