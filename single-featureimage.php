<?php get_header(); ?>

<div class="container" id="logoSection">
			
	<div id="mainLogo" class="row noSlider">
	
		<section class="sevencol">
			<h1 id="pageTitle"><a href="<?php echo get_category_link( 3); ?>" title="News">News</a></h1>
		</section>
		
		<section class="fivecol last">
			<h2 id="utmsasSecondary"><a href="<?php echo site_url(); ?>" title="University of Texas Medical School at Houston" target="_parent"><img src="<?php bloginfo('template_directory'); ?>/img/logo/medschool-logo-large-current.png" alt="University of Texas Medical School at Houston" /></a></h2>
		</section>
		
	</div><!--end row-->
	
</div><!-- ending #logoSection-->
		
<?php include('includes/mission-nav.php'); ?>

<div class="container" id="mainContent">
	
	<div class="row">
	
		<section id="mainArticle" class="twelvecol last">
					
			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>	
			
			
				<article <?php post_class() ?> id="post-<?php the_ID(); ?>">
					
					<?php utms_set_post_views(get_the_ID()); //Set for popular posts?>
					
					<p><?php edit_post_link('Edit this entry',''); ?></p>
					
					<h1><?php the_title(); ?></h1>
					
					<p class="tags"><?php the_tags( '', '', '' ); ?></p>
					<!--<p><?php the_tags(); ?></p>-->
					
					<!-- AddThis Button BEGIN -->
					<div class="addthis_toolbox addthis_default_style ">
						<a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>
						<a class="addthis_button_tweet"></a>
						<a class="addthis_button_google_plusone" g:plusone:size="medium"></a>
						<?php //<a class="addthis_button_pinterest_pinit"></a> ?>
						<a class="addthis_counter addthis_pill_style"></a>
					</div>
					<script type="text/javascript" src="https://s7.addthis.com/js/300/addthis_widget.js#pubid=utmedschool"></script>
					<!-- AddThis Button END -->
					
		       		<div class="entry">
		       		
		       		<?php
		                if(has_post_thumbnail()) { ?>
		                    <div class="entry-thumbnail">
		                        <?php the_post_thumbnail();?>
		                    </div>
		            <?php } ?>
		            
		            <?php the_content();?>
		            
		            <?php $cf = get_post_custom(); 
			            $photographer = $cf['_info_photographer'][0];
		            ?>
					
					<?php if(!empty($photographer)) {?>
							<p class="alignright">&mdash; <?php echo $photographer; ?></p>
					<?php } ?>
		       		
		       		</div>
				
				</article>
				
				<?php comments_template(); ?>
				
				<?php endwhile; endif; ?>
			
		</section><!-- ending #contentDiv -->
		
	</div>
	
</div><!--ending #mainContent-->

<?php get_footer(); ?>