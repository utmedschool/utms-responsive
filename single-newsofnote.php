<html>

<head>
	<title>UTHealth Medical School - News of Note for <?php echo the_title(); ?></title>
	<meta name="author" content="Office of Communications, UTHealth Medical School">
	<meta name="description" content="News of Note is a curated content enewsletter for UTHealth Medical School">
	<meta name="Copyright" content="Copyright UTHealth Med School 2013. All Rights Reserved.">
	<link rel="shortcut icon" href="<?php bloginfo('template_directory'); ?>/img/common/favicon.ico">		 
	<style>
		h2.headlinelink a:hover, a:hover{ color: #39abe7 !important;}
		.readmorelink a:hover{ color: #0a91bc !important;}
	</style>
	<?php wp_head(); ?>
	
</head>

<body style="margin-top:0; margin-bottom:0; margin-right:0; margin-left:0; padding-top:0; padding-bottom:0; padding-right:0; padding-left:0;" >
	<?php if ( have_posts() ): while ( have_posts() ) : the_post(); ?>
	<!-- TOP SECTION -->
	<table border="0" cellpadding="0" cellspacing="0" width="100%">		
		<tr>
			<td bgcolor="#f6f6f6" valign="top">
				<div align="center" style="padding-bottom:0; padding-left:15; padding-right:15; padding-top:0;">
					<table border="0" cellpadding="0" cellspacing="0" width="600">
						<tr>
							<td width="460" align="left" style="color:#333333; font-family:Helvetica,Arial,sans-serif; font-size:12px; line-height:1.5em; vertical-align:text-top;padding-bottom:10; padding-left:0; padding-right:0; padding-top:10;">Having trouble viewing this email? <a href="<?php the_permalink(); ?>?utm_source=newsletter&utm_medium=email&utm_campaign=NewsofNoteDaily" style="color:#333333;" title="Open it in your browser"><em>Open it in your browser</em></a>.</td>
							<td width="140" align="right" style="color:#0a91bc; font-family:Helvetica,Arial,sans-serif; font-size:14px; line-height:1.5em; vertical-align:text-top;padding-bottom:10; padding-left:0; padding-right:0; padding-top:10px;">
							<?php the_date('F d, Y', '<strong>', '</strong>'); ?></td>
						</tr>
					</table>
				</div>
			</td>
		</tr>
	</table>
	<!-- LOGO SECTION -->
	<table border="0" cellpadding="0" cellspacing="0" width="100%">
		<tr>
			<td bgcolor="#ffffff" valign="top">
				<div align="center" style="padding-bottom:0; padding-left:15; padding-right:15; padding-top:0;">
					<table border="0" cellpadding="0" cellspacing="0" width="600">
						<tr>
							<td width="600" align="left" style="color:#2a2d31; font-family:Helvetica,Arial,sans-serif; font-size:12px; line-height:1.5em; vertical-align:text-top;padding-bottom:10; padding-left:0; padding-right:0; padding-top:10;"><a href="<?php echo get_post_type_archive_link( 'newsofnote' ); ?>?utm_source=newsletter&utm_medium=email&utm_campaign=NewsofNoteDaily" style="color:#3589AE;text-decoration:underline;" title="View NewsofNote Archives"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/newsofnote/news-of-note-logo-2013.jpg" alt="News of Note" width="295" height="45" border="0" /></a></td>
						</tr>
						<tr>
							<td width="600" align="right" style="color:#999999; font-family:Georgia, Times New Roman, serif; font-size:12px; line-height:1.5em; vertical-align:text-top;padding-bottom:10; padding-left:0; padding-right:0; padding-top:10;">
							<em>A curated content enewsletter for <a href="<?php echo network_site_url(); ?>?utm_source=newsletter&utm_medium=email&utm_campaign=NewsofNoteDaily" title="UTHealth Medical School" style="color:#999999; font-weight:bold; text-decoration:none;">UTHealth Medical School</a> since 2000</em></td>   
						</tr>
					</table>
				</div>
			</td>
		</tr>
	</table>
	<!-- SOCIAL MEDIA SECTION -->
			
	<table border="0" cellpadding="0" cellspacing="0" width="100%">
		<tr>
			<td valign="top">
				<div align="center">
					<table border="0" cellpadding="0" cellspacing="0" width="600">						
						<tr>
							<td colspan="3" width="100%"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/newsofnote/double-border-social-media.jpg" alt="" width="600" height="11" border="0" /></td>
						</tr>
						<tr>
							<td width="200" align="left" style="color:#2a2d31; font-family:Helvetica,Arial,sans-serif; font-size:12px; line-height:1.5em; vertical-align:middle;">
							<a href="http://www.facebook.com/utmedschool" title="Visit our Facebook Page"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/newsofnote/icons/icon-facebook.jpg" alt="Visit our Facebook Page" width="16" height="16" border="0" style="margin-bottom:3px; margin-top:3px;" /></a>&nbsp;&nbsp;
							<a href="http://twitter.com/utmedschool" title="Visit our Twitter Page"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/newsofnote/icons/icon-twitter.jpg" alt="Visit our Twitter Page" width="16" height="16" border="0" style="margin-bottom:3px; margin-top:3px;" /></a>&nbsp;&nbsp; 
							<a href="http://vimeo.com/utmedschool" title="Visit our Vimeo Page"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/newsofnote/icons/icon-vimeo.jpg" alt="Visit our Vimeo Page" width="16" height="16" border="0" style="margin-bottom:3px; margin-top:3px;" /></a>&nbsp;&nbsp; 
							<a href="http://www.youtube.com/utmedicalschool" title="Visit our YouTube Page"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/newsofnote/icons/icon-youtube.jpg" alt="Visit our YouTube Page" width="16" height="16" border="0" style="margin-bottom:3px; margin-top:3px;" /></a>&nbsp;&nbsp; 
							<a href="http://instagram.com/utmedschool" title="Visit our Instagram Page"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/newsofnote/icons/icon-instagram.jpg" alt="Visit our Instagram Page" width="16" height="16" border="0" style="margin-bottom:3px; margin-top:3px;" /></a></td>
							<td width="200" align="left"><a href="https://med.uth.edu/events/" style="color:#333333; font-family:Helvetica,Arial,sans-serif; font-size:12px; line-height:1.5em; vertical-align:middle; text-decoration:none; margin-bottom:3px; margin-top:3px;" title="View Events Calendar"><strong>View Events Calendar <span style="font-size:16px;">&#187;</span></strong></a></td>
							<td width="200" align="right"><a href="<?php echo get_category_link(3); ?>?utm_source=newsletter&utm_medium=email&utm_campaign=NewsofNoteDaily"  style="color:#333333; font-family:Helvetica,Arial,sans-serif; font-size:12px; line-height:1.5em; padding-bottom:10px; padding-left:0px; padding-right:0px; padding-top:10px; text-decoration:none; vertical-align:text-top;" title="View Medical School News"><strong>View Medical School News <span style="font-size:16px;">&#187;</span></strong></a></td>
						</tr>						
						<tr>
							<td colspan="3" width="100%"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/newsofnote/double-border-social-media.jpg" alt="" width="600" height="11" border="0" /></td>
						</tr>
					</table>
				</div>
			</td>
		</tr>
	</table>

<div align="center">
	<table width="600" align="center" border="0" cellpadding="0" cellspacing="0" style="border:none;">
		<tr>
			<td>&nbsp;</td>
		</tr>
		
		<?php
		$newsofnotearray = get_post_meta($post->ID, '_details', true);
		// print_r(get_post_meta($post->ID, '_details', true));
        foreach ( $newsofnotearray as $item ) {
            	$title = $item['_title'];
            	$excerpt = $item['_excerpt'];
            	$url = $item['_url']; ?>
	    <tr>
			<td align="left" valign="top" style="background-color:#FFFFFF; padding-bottom:20; font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#333333;">        
		        <h2 style="margin-bottom:5; margin-top:0;"><a href="<?php echo $url; ?>" target="_blank" style="color:#565656; font-family:Georgia, Times New Roman, serif; font-size:24px; text-decoration:none;" title="External Link to <?php echo $title; ?>"><?php echo $title; ?></a></h2>
				<p style="color:#333333; font-size:12px; margin-top:0;">
				<?php echo $excerpt; ?> <em><a href="<?php echo $url; ?>" target="_blank" title="External Link to <?php echo $title; ?>" style="color:#39abe7; text-decoration:none;">Read&nbsp;full&nbsp;story&nbsp;&#187;</a></em></p>
				
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/newsofnote/double-border-stories.jpg" alt="" width="600" height="11" border="0" />	
			</td>
	    </tr>
	    
	    <?php } ?>
    
    
		<tr>
			<td align="left" valign="top" style="background-color:#FFFFFF; padding-bottom:20; font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#333333;">
				<p style="color:#333333; font-size:12px;">Some publications may require readers to register for free.</p>
			</td>    
		</tr>
    
	</table>
</div>

	<!-- FOOTER SECTION -->
	<table border="0" cellpadding="0" cellspacing="0" width="100%">
		<tr>
			<td bgcolor="#828485" valign="top">
				<div align="center" style="padding-bottom:0; padding-left:15; padding-right:15; padding-top:0;">
					<table border="0" cellpadding="0" cellspacing="0" width="600">
						<tr>
							<td width="300" align="left" valign="top" style="color:#ffffff; font-family:Helvetica,Arial,sans-serif; font-size:12px; line-height:1.5em; padding-bottom:10; padding-left:0; padding-right:0; padding-top:10;">Brought to you by<br/>
							<a href="https://med.uth.edu/comm" style="color:#3589AE;text-decoration:underline;" title="Visit Office of Communication's website"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/newsofnote/ooc-logo.jpg" alt="Office of Communications logo" width="190" height="51" border="0" /></a></td>
							<td width="300" align="right" valign="top" style="color:#ffffff; font-family:Helvetica,Arial,sans-serif; font-size:14px; line-height:1.5em; padding-bottom:10; padding-left:0; padding-right:0; padding-top:10;"><a href="<?php echo network_site_url(); ?>?utm_source=newsletter&utm_medium=email&utm_campaign=NewsofNoteDaily"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/newsofnote/medical-school-logo.jpg" alt="UTHealth Medical School logo" width="238" height="81" border="0" /></a></td>
						</tr>
						<tr>
							<td colspan="2" width="600" align="left" style="color:#ffffff; font-family:Helvetica,Arial,sans-serif; font-size:12px; line-height:1.5em; vertical-align:text-top;padding-bottom:10; padding-left:0; padding-right:0; padding-top:10;">
								<strong>Office of Communications</strong> | <a href="http://med.uth.edu/comm" style="color:#ffffff; text-decoration:none;" title="Visit the website">https://med.uth.edu/comm</a><br/>
								NewsofNote is a daily curated content enewsletter that provides medical, health and higher education news.<br/>
								To subscribe to NewsofNote please visit <a href="https://med.uth.edu/manage-subscriptions" style="color:#ffffff; text-decoration:none;" title="Visit the website">https://med.uth.edu/manage-subscriptions</a>.
							</td>
						</tr>
					</table>
				</div>
			</td>
		</tr>
	</table>
	
	<?php endwhile; ?>
	<?php else: ?>
	<h2>No News of Note to display</h2>
	<?php endif; ?>
	
	<?php wp_footer(); ?>
	
</body>
</html>