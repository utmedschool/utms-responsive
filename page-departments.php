<?php
/*
Template Name: Departments Page
*/

 get_header(); ?>

<div class="container" id="logoSection">
			
	<div id="mainLogo" class="row">
	
		<section class="sevencol">
			<h1 id="pageTitle"><?php the_title(); ?></h1>
		</section>
		
		<section class="fivecol last">
			<h2 id="utmsasSecondary"><a href="<?php echo site_url(); ?>" title="University of Texas Medical School at Houston" target="_parent"><img src="<?php bloginfo('template_directory'); ?>/img/logo/medschool-logo-large-current.png" alt="University of Texas Medical School at Houston" /></a></h2>
		</section>
		
	</div><!--end row-->
	
</div><!-- ending #logoSection-->
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
<div id="rslider" class="container">
	<div class="row">
		<div id="flexcontainer" class="twelvecol last">
			<div class="flexslider loadingif">
				<ul class="slides">
					<?php 
						$featureargs = array(
						'post_type' => 'featureimage',
						'posts_per_page' => 5,
						'feature_category'	=> 'departments'
						);
						$featurequery = new WP_Query( $featureargs );
						if ( $featurequery->have_posts() ) : while ( $featurequery->have_posts() ) : $featurequery->the_post(); 
							
							$cf = get_post_custom($post->ID);
							$directlink = $cf['_info_link_url'][0];	
							$pagelink = $cf['_info_page_select'][0];	
							$postlink = $cf['_info_post_select'][0];
							
							if (!empty($directlink)){
								$finalurl = get_permalink($directlink);
							} elseif ( (empty($directlink)) && (!empty($pagelink)) ) {
								$finalurl = get_permalink($pagelink);
							} elseif ( (empty($directlink)) && (empty($pagelink)) &&  (!empty($postlink)) ) {
								$finalurl = get_permalink($postlink);
							} else {
								$finalurl = '#';
							}
					?>
							<li><a href="<?php echo $finalurl; ?>"><?php the_post_thumbnail(); ?></a></li>
					<?php endwhile; else: ?>
							<li><a><img src="<?php bloginfo('template_directory');?>/img/slider-images/ms-building-ustxflag.jpg" alt="Medical School Building External Shot"></a></li>
					<?php endif;
						wp_reset_query();
					?>
					
				</ul>
			</div><!--ends flexslider-->
		</div><!--ends flexcontainer-->
	</div><!--ends .row-->
</div><!--ends rslider.container-->
<?php endwhile; endif; ?>
		
<?php include('includes/mission-nav.php'); ?>

<div class="container" id="mainContent">
	
	<div class="row">
	
		<ul class="threecol">
			
			<?php dynamic_sidebar( 'left-sidebar-widgets' ); ?> 
		
		</ul>
		
		<div class="ninecol last">
		
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			<?php the_content(); ?>
		<?php endwhile; endif; ?>
		
			<ul class="sixcol">
				<?php dynamic_sidebar( 'mid-sidebar-widgets' ); ?> 
			</ul>
		
			<ul class="sixcol last">
				<?php get_sidebar(); ?>
				<?php include (TEMPLATEPATH . '/includes/righticon-buttons.php'); ?>
			</ul>
			
		</div>
	
	</div>
	
</div><!--ending #mainContent-->

<?php get_footer(); ?>