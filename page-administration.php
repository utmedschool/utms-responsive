<?php 
/*
Template Name: UTMS Administration
*/

get_header(); ?>

<div class="container" id="logoSection">
			
	<div id="mainLogo" class="row">
	
		<section class="sevencol">
			<h1 id="pageTitle">Administration</h1>
		</section>
		
		<section class="fivecol last">
			<h2 id="utmsasSecondary"><a href="<?php echo site_url(); ?>" title="University of Texas Medical School at Houston" target="_parent"><img src="<?php bloginfo('template_directory'); ?>/img/logo/medschool-logo-large-current.png" alt="University of Texas Medical School at Houston" /></a></h2>
		</section>
		
	</div><!--end row-->
	
</div><!-- ending #logoSection-->

<div id="rslider" class="container">
	<div class="row">
		<div id="flexcontainer" class="twelvecol last">
			<div class="flexslider loadingif">
				<ul class="slides">
					<li><?php the_post_thumbnail( 'static-image' ); ?></li>
				</ul>
			</div><!--ends flexslider-->
		</div><!--ends flexcontainer-->
	</div><!--ends .row-->
</div><!--ends rslider.container-->

<?php include('includes/mission-nav.php'); ?>

<div class="container" id="mainContent">
	
	<div class="row">
	
		<section class="threecol">
			<?php include('includes/left-sidebar.php'); ?>
		</section>
	
		<section id="mainArticle" class="sixcol">
			
			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
				<article <?php post_class() ?> id="post-<?php the_ID(); ?>">
				<p><?php edit_post_link('Edit this entry',''); ?></p>
				<h1><?php the_title(); ?></h1>
	       		<?php the_content();?>
				</article>
			
			<?php endwhile; endif; ?>
		</section><!-- ending #contentDiv -->
		
		<aside class="threecol last">		
			<?php get_sidebar(); ?>
			<?php include (TEMPLATEPATH . '/includes/righticon-buttons.php'); ?>
		</aside>
		
	
	</div>
	
</div><!--ending #mainContent-->
	
<?php get_footer(); ?>
