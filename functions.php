<?php
	
	// Add RSS links to <head> section
	//	automatic_feed_links();
	// add_theme_support( 'automatic-feed-links' );
	
	// Load jQuery
	if ( !function_exists('core_mods') ) {
		function core_mods() {
			if ( !is_admin() ) {
				wp_deregister_script('jquery');
				wp_register_script('jquery', ("https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"), false);
				wp_enqueue_script('jquery');
			}
		}
		// depreceated core_mods();
		add_action('wp_enqueue_scripts', 'core_mods');
	}
		
		
	// Clean up the <head>
	function removeHeadLinks() {
    	remove_action('wp_head', 'rsd_link');
    	remove_action('wp_head', 'wlwmanifest_link');
    }
    add_action('init', 'removeHeadLinks');
    remove_action('wp_head', 'wp_generator');
    
    if (function_exists('register_sidebar')) {
    	register_sidebar(array(
    		'name' => __('Sidebar Widgets','UTHealth' ),
    		'id'   => 'sidebar-widgets',
    		'description'   => __( 'These are widgets for the sidebar.','UTHealth' ),
    		'before_widget' => '<div id="%1$s" class="widget %2$s">',
    		'after_widget'  => '</div>',
    		'before_title'  => '<h2>',
    		'after_title'   => '</h2>'
    	));
    	
    	register_sidebar(array(
    		'name' => __('Left Sidebar Widgets','UTHealth' ),
    		'id'   => 'left-sidebar-widgets',
    		'description'   => __( 'These are widgets for the left sidebar.','UTHealth' ),
    		'before_widget' => '<div id="%1$s" class="widget %2$s">',
    		'after_widget'  => '</div>',
    		'before_title'  => '<h2>',
    		'after_title'   => '</h2>'
    	));
    	
    	register_sidebar(array(
    		'name' => __('Mid Sidebar Widgets','UTHealth' ),
    		'id'   => 'mid-sidebar-widgets',
    		'description'   => __( '','UTHealth' ),
    		'before_widget' => '<div id="%1$s" class="widget %2$s">',
    		'after_widget'  => '</div>',
    		'before_title'  => '<h2>',
    		'after_title'   => '</h2>'
    	));
    	
    	
    }
    
    add_theme_support( 'post-formats', array('aside', 'gallery', 'link', 'image', 'quote', 'status', 'audio', 'chat', 'video')); // Add 3.1 post format theme support.
    
//	Add thumbnail support
if ( function_exists('add_theme_support') )
add_theme_support('post-thumbnails');
add_image_size( 'sliderlarge', 890, 440, true ); // Homepage Slider Image
add_image_size( 'homenews-main', 100, 120 ); // Main Scoop News
add_image_size( 'homenews-second', 55, 66 ); // Second Scoop News
add_image_size( 'home-downloads', 116, 137, true); // Downloadable
add_image_size( 'slidernav-secondary', 55, 55, true ); // Second Scoop News
add_image_size( 'static-image', 1440, 440 ); //Full Static Image
add_image_size( 'newspage-main', 124, 160 ); //News Page Main Image
add_image_size( 'wildart-full', 615, 420 ); //News Page Wild Art Full Resolution
add_image_size( 'wildart-thumb', 150, 102 ); //Wildart Thumb Full Resolution
add_image_size('spotlight-img', 270, 170, true ); //In the Spotlight Featured Image
add_image_size('memoriam-img', 85, 85, true ); //In Memoriam Featured Image

//	Add external URL
function open_ext($url){
$url = $url;
$content = wp_remote_fopen($url);
if ($content<>'') {
return $content;
} else {
	echo 'There was an error loading a few items, please refresh again!';
	}
}

//Turn a category ID to a Name
function cat_id_to_name($id) {
	foreach((array)(get_categories()) as $category) {
    	if ($id == $category->cat_ID) { return $category->cat_name; break; }
	}
}

// Add navigation support
if ( function_exists('add_theme_support') )
add_theme_support( 'menus' );

// Add default posts and comments RSS feed links to head
if ( function_exists('add_theme_support') )
add_theme_support( 'automatic-feed-links' );

// Display home page link in custom menu
function home_page_menu_args( $args ) {
$args['show_home'] = true;
return $args;
}
add_filter('wp_page_menu_args', 'home_page_menu_args');


//Changing wp-login.php to access
add_filter('site_url',  'wplogin_filter', 10, 3);
function wplogin_filter( $url, $path, $orig_scheme )
{
 $old  = array( "/(wp-login\.php)/");
 $new  = array( "access");
 return preg_replace( $old, $new, $url, 1);
}

// Customize login styles
function utms_custom_login_styles(){
?><style type="text/css">

	/* Change login page background */
	/* Make sure you change image paths to yours */
	body.login{ background: #000 url("<?php echo get_stylesheet_directory_uri(); ?>/img/access-background.jpg") 50% 0px no-repeat fixed !important; }
 
	/* Make sure you change image paths to yours */
	body.login h1 a { background: url("<?php echo get_stylesheet_directory_uri(); ?>/img/logo/utms-logo-white.png") no-repeat top center; width:320px; height: 80px; }
 
	/* Optional : Change link color & shadow if needed */
	.login #nav a, .login #backtoblog a { color: #999!important; text-decoration:none; text-shadow:#000 1px 1px 0; }
	.login #nav a:hover, .login #backtoblog a:hover { color: #fff!important; text-decoration:none; text-shadow:#000 1px 1px 0; }
 
	/* Optional : Add some shadow to message boxes and login box */
	.login form, p.message {
	-moz-box-shadow: rgba(0,0,0,0.5) 0 2px 8px;
	-webkit-box-shadow: rgba(0,0,0,0.5) 0 2px 8px;
	box-shadow: rgba(0,0,0,0.5) 0 2px 8px;
	}
	</style><?php
	}
add_action('login_head', 'utms_custom_login_styles');

// Add a login message
function utms_login_message($msg){
    return $msg . "<p class=\"message\">Welcome to <strong>".get_bloginfo('name')."</strong>. Unauthorized use is prohibited. <strong>Misuse is subject to criminal prosecution.</strong></p>";
} add_action('login_message', 'utms_login_message');


require_once('includes/gravityform.php'); /* This is for the events autosubmission from GF to Events Calendar*/

/* 	Popular Posts without Plugin-- Adds a custom field 
	http://www.wpbeginner.com/wp-tutorials/how-to-track-popular-posts-by-views-in-wordpress-without-a-plugin */

function utms_set_post_views($postID) {
    $count_key = 'utms_post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
    }else{
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
}
//To keep the count accurate, lets get rid of prefetching
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);

//Adding the tracker in your header by using wp_head hook.
function utms_track_post_views ($post_id) {
    if ( !is_single() ) return;
    if ( empty ( $post_id) ) {
        global $post;
        $post_id = $post->ID;    
	    }
	    utms_set_post_views($post_id);
	}
	
add_action( 'wp_head', 'utms_track_post_views');


	// Excluding newsofnote from archive pages
	//-490 removed for newsofnote, all newofnotes are now in trash after the CPT addition
	
	function exclude_non($query) {
	//if ( $query->is_date) {
	if (is_archive() && !is_category('news')) {
	        $query->set('cat', '-555');
	}
	return $query;
	}
	add_filter('pre_get_posts', 'exclude_non');



function exclude_thumbnail_from_gallery($null, $attr)
{
    if (!$thumbnail_ID = get_post_thumbnail_id())
        return $null; // no point carrying on if no thumbnail ID

    // temporarily remove the filter, otherwise endless loop!
    remove_filter('post_gallery', 'exclude_thumbnail_from_gallery');

    // pop in our excluded thumbnail
    if (!isset($attr['exclude']) || empty($attr['exclude']))
        $attr['exclude'] = array($thumbnail_ID);
    elseif (is_array($attr['exclude']))
        $attr['exclude'][] = $thumbnail_ID;

    // now manually invoke the shortcode handler
    $gallery = gallery_shortcode($attr);

    // add the filter back
    add_filter('post_gallery', 'exclude_thumbnail_from_gallery', 10, 2);

    // return output to the calling instance of gallery_shortcode()
    return $gallery;
}
add_filter('post_gallery', 'exclude_thumbnail_from_gallery', 10, 2);

	include('includes/slideshow.php');

	/*---------------------------------------------------------------
	---------------------------------------------------------------
	---------------------------------------------------------------
	---------------------------------------------------------------
				 Additions for CPT Helper Class
				 	July 12, 2013 
	---------------------------------------------------------------
	---------------------------------------------------------------
	---------------------------------------------------------------
	---------------------------------------------------------------
	---------------------------------------------------------------*/
				 	

	// DEFINITIONS
	define( 'THEME_URI', get_stylesheet_directory() );
	define( 'THEME_INCLUDE', THEME_URI . '/includes' );

	//Include Custom Post Type Helper Class
	require_once( THEME_INCLUDE . '/helper-class/2.9.4/cuztom.php');

	// INCLUDE MOBILE DETECTION FUNCTION
//  require_once ( THEME_INCLUDE . '/mobiledetect/Mobile_Detect.php' );
  // use $detect = new Mobile_Detect; to access
	
	
	//Include the CPTs for this theme
	include( THEME_INCLUDE. '/cpt/newsofnote.php');
	include( THEME_INCLUDE. '/cpt/featureimage.php');


	// REMOVE DEFAULT SETTINGS OF IMAGES TO AUTOMATICALLY LINK TO SELF
	update_option('image_default_link_type','none');

?>