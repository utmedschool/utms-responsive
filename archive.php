<?php get_header(); ?>

<div class="container" id="logoSection">
			
	<div id="mainLogo" class="row noSlider">
	<?php if (have_posts()) : ?>
		<?php $post = $posts[0]; // Hack. Set $post so that the_date() works. ?>
		
		<section class="sevencol">
		<?php /* If this is a category archive */ if (is_category()) { ?>
			<h1 id="pageTitle"><?php single_cat_title(); ?> Archive</h1>
	
		<?php /* If this is a tag archive */ } elseif( is_tag() ) { ?>
			<h1 id="pageTitle"><?php single_tag_title(); ?> Archive</h1>
	
		<?php /* If this is a daily archive */ } elseif (is_day()) { ?>
			<h1 id="pageTitle"><?php the_time('F jS, Y'); ?> News</h1>
	
		<?php /* If this is a monthly archive */ } elseif (is_month()) { ?>
			<h1 id="pageTitle"><?php the_time('F'); ?> Archive</h1>
	
		<?php /* If this is a yearly archive */ } elseif (is_year()) { ?>
			<h1 id="pageTitle"><?php the_time('Y'); ?> Archive</h1>
	
		<?php /* If this is an author archive */ } elseif (is_author()) { ?>
			<h1 id="pageTitle">Author Archives</h1>
	
		<?php /* If this is a paged archive */ } elseif (isset($_GET['paged']) && !empty($_GET['paged'])) { ?>
			<h1 id="pageTitle">Blog Archives</h1>
		
		<?php } ?>
		</section>
		
		<section class="fivecol last">
			<h2 id="utmsasSecondary"><a href="<?php echo site_url(); ?>" title="University of Texas Medical School at Houston" target="_parent"><img src="<?php bloginfo('template_directory'); ?>/img/logo/medschool-logo-large-current.png" alt="University of Texas Medical School at Houston" /></a></h2>
		</section>
		
	</div><!--end row-->
	
</div><!-- ending #logoSection-->

<?php include('includes/mission-nav.php'); ?>

<div id="above-the-fold" class="container">
	<div class="row">
		<div class="sevencol">
			
		
					<?php /* If this is a category archive */ if (is_category()) { ?>
						<p class="section-headline">Archive for the <strong class="darkblue">&#8216;<?php single_cat_title(); ?>&#8217;</strong> Category
		
					<?php /* If this is a tag archive */ } elseif( is_tag() ) { ?>
						<p class="section-headline">Posts Tagged <strong class="darkblue">&#8216;<?php single_tag_title(); ?>&#8217;</strong>
		
					<?php /* If this is a daily archive */ } elseif (is_day()) { ?>
						<p class="section-headline">Archive for <strong class="darkblue"><?php the_time('F jS, Y'); ?></strong>
		
					<?php /* If this is a monthly archive */ } elseif (is_month()) { ?>
						<p class="section-headline">Archive for <strong class="darkblue"><?php the_time('F, Y'); ?></strong>
		
					<?php /* If this is a yearly archive */ } elseif (is_year()) { ?>
						<p class="section-headline">Archive for <strong class="darkblue"><?php the_time('Y'); ?></strong>
		
					<?php /* If this is an author archive */ } elseif (is_author()) { ?>
						<p class="section-headline">Author Archive
		
					<?php /* If this is a paged archive */ } elseif (isset($_GET['paged']) && !empty($_GET['paged'])) { ?>
						<p class="section-headline">Blog Archives
					
					<?php } ?>
					
					<select name="archive-dropdown" onchange="document.location.href=this.options[this.selectedIndex].value;">
					  <option value=""><?php echo esc_attr( __( 'Select Month' ) ); ?></option> 
					  <?php wp_get_archives( 'type=monthly&format=option&show_post_count=0' ); ?>
					</select>
					
					// <a href="http://med.uth.tmc.edu/comm/Scoop/archive/2014.html" title="scoop Archives">old <span class="scoop-logo scoop-logo-small">scoop</span> archives</a></p>
					
					<?php
						global $wp_query;
						$big = 99999999;
						echo paginate_links(array(
						'base' => str_replace($big, '%#%', get_pagenum_link($big)),
						'format' => '?paged=%#%',
						'total' => $wp_query->max_num_pages,
						'current' => max(1, get_query_var('paged')),
						'show_all' => false,
						'end_size' => 2,
						'mid_size' => 3,
						'prev_next' => true,
						'prev_text' => 'Prev',
						'next_text' => 'Next',
						'type' => 'list'
						));
					?>
					
					<ul class="news-list newsarchive">
					
					<?php while (have_posts()) : the_post(); ?>
							
								<li <?php post_class(); ?>>
									<h2><a href="<?php the_permalink(); ?>" title="Read more about <?php the_title(); ?>"><?php the_title(); ?></a></h2>
									<p class="tags"><?php the_tags( '', '', '' ); ?></p>
								</li>
		
					<?php endwhile; ?>
					
					</ul>
					
					<?php
						global $wp_query;
						$big = 99999999;
						echo paginate_links(array(
						'base' => str_replace($big, '%#%', get_pagenum_link($big)),
						'format' => '?paged=%#%',
						'total' => $wp_query->max_num_pages,
						'current' => max(1, get_query_var('paged')),
						'show_all' => false,
						'end_size' => 2,
						'mid_size' => 3,
						'prev_next' => true,
						'prev_text' => 'Prev',
						'next_text' => 'Next',
						'type' => 'list'
						));
					?>
					
			<?php else : ?>
		
				<h2>Nothing found</h2>
		
			<?php endif; ?>
		</div>
		<div class="fivecol last">
			
			
		
			<p class="section-headline">wild<strong class="darkblue">art</strong> // <a href="<?php echo get_category_link(479); ?>" title="Wild Art Archives">archives</a></p>
			<section>
				<ul class="wild-art-section wildartarchive wildartsidebar">
				
				<?php $recent = new WP_Query( array( 
								'category_name'		=> 'wildart',
								'posts_per_page'			=> 2
								) );
				while ($recent->have_posts()) : $recent->the_post(); ?>
					<li>
						<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
						<h2><?php the_title(); ?></h2>
						<?php the_post_thumbnail( 'wildart-thumb' ); ?>	
						</a>
					</li>
					
				<?php endwhile; ?>
								
				<?php wp_reset_postdata(); ?>
				
				</ul>
				
			</section>
			
			
		</div>
	</div><!--ending row-->
</div><!-- /ending above the fold container-->

<?php get_footer(); ?>
