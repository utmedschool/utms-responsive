<!doctype html>
<html class="no-js" <?php language_attributes(); ?>>

<head>

	<meta charset="<?php bloginfo('charset'); ?>" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	
	<?php if (is_search()) { ?>
	<meta name="robots" content="noindex, nofollow" /> 
	<?php } ?>
	
	<title>
		   <?php
		      if (function_exists('is_tag') && is_tag()) {
		         single_tag_title("Tag Archive for &quot;"); echo '&quot; - '; }
		      elseif (is_archive()) {
		         wp_title(''); echo ' - '; }
		      elseif (is_search()) {
		         echo 'Search for &quot;'.wp_specialchars($s).'&quot; - '; }
		      elseif (!(is_404()) && (is_single()) || (is_page())) {
		         wp_title(''); echo ' - '; }
		      elseif (is_404()) {
		         echo 'Not Found - '; }
		      if (is_home()) {
		         bloginfo('name'); echo ' - Home '; }
		      else {
		          bloginfo('name'); }
		      if ($paged>1) {
		         echo ' - page '. $paged; }
		   ?>
	</title>
		   
	<?php if (is_home()) { ?>
		<meta name="description" content="<?php bloginfo('description'); ?>">
	<?php } ?>
	
	<meta name="google-site-verification" content="TimdGzuLl1Yw4Dq45FMXkoWoVgJiZXFGs9itQCqhjXg">	
	<meta name="author" content="Office of Communication, Medical School">
	<meta name="Copyright" content="Copyright UTHealth Medical School <?php echo date('Y'); ?>. All Rights Reserved.">
	
	<link type="text/plain" rel="author" href="<?php echo get_template_directory_uri(); ?>/humans.txt" />

	<meta name="viewport" content="width=device-width, maximum-scale=1.0" />
	
	<link rel="shortcut icon" href="<?php bloginfo('template_directory'); ?>/img/common/favicon.ico">
	<link rel="apple-touch-icon" href="<?php echo get_stylesheet_directory_uri(); ?>/img/appletouch/apple-touch-icon.png" />
	<link rel="apple-touch-icon" sizes="57x57" href="<?php echo get_stylesheet_directory_uri(); ?>/img/appletouch/apple-touch-icon-57x57.png" />
	<link rel="apple-touch-icon" sizes="72x72" href="<?php echo get_stylesheet_directory_uri(); ?>/img/appletouch/apple-touch-icon-72x72.png" />
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo get_stylesheet_directory_uri(); ?>/img/appletouch/apple-touch-icon-76x76.png" />
	<link rel="apple-touch-icon" sizes="114x114" href="<?php echo get_stylesheet_directory_uri(); ?>/img/appletouch/apple-touch-icon-114x114.png" />
	<link rel="apple-touch-icon" sizes="120x120" href="<?php echo get_stylesheet_directory_uri(); ?>/img/appletouch/apple-touch-icon-120x120.png" />
	<link rel="apple-touch-icon" sizes="144x144" href="<?php echo get_stylesheet_directory_uri(); ?>/img/appletouch/apple-touch-icon-144x144.png" />
	<link rel="apple-touch-icon" sizes="152x152" href="<?php echo get_stylesheet_directory_uri(); ?>/img/appletouch/apple-touch-icon-152x152.png" />
		 
	<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>">
	<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/flexslider.css" type="text/css">
	
	<script src="<?php bloginfo('template_directory'); ?>/js/modernizr-2.0.6.js"></script>
	
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
	<!--<?php if ( is_singular() ) wp_enqueue_script( 'comment-reply' ); ?>-->
	<?php wp_head(); ?>
	<!--<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS Feed" href="<?php bloginfo('rss2_url'); ?>" />-->
	
	<!--[if lt IE 9]>
		<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/css3-mediaqueries.js"></script>
	<![endif]-->
	
	<!--[if lte IE 9]>
		<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/ie.css" type="text/css" media="screen" />
	<![endif]-->


	<script type="text/javascript">
		window.addEventListener("load",function() {
		  setTimeout(function(){
		    window.scrollTo(0, 1);
		  }, 0);
		});
	</script>
	
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
	  ga('create', 'UA-27035114-1', 'auto');
  	  ga('require', 'displayfeatures');
	  ga('send', 'pageview');
	
	</script>
	<?php //flush(); ?>

</head>

<body <?php body_class(); ?>>
	
	<?php // For accessibility and compliance to Section 508 ?>
	
	<div id="skip">
		<a href="#mainContent" accesskey="m">Skip to Main Content <small>(accesskey m)</small></a> <a href="#linksResources" accesskey="n">Skip to Navigation <small>(accesskey n)</small></a>
	</div> <!-- END #skip -->
	
	<?php //echo open_ext("https://med.uth.edu/utms-universal/utms-superinclude-topnav.php"); ?>

<header id="superTop">

	<section id="superHolder">
	
		<ul id="supernavRight">
			<li><a href="http://www.uth.edu" target="_blank" title="UTHealth Health Science Center">UTHealth</a></li>
			<li><a href="https://med.uth.edu" target="_blank" title="UTHealth Medical School">Medical School</a></li>
			<li><a href="http://peopledirectory.uth.tmc.edu/peopledirectory/index.jsp" target="_blank" title="Directory">People</a></li>
			<li><a href="https://webmail.uth.tmc.edu/" target="_blank" title="Webmail">Webmail</a></li>
			<li><a href="https://med.uth.edu/events" title="Medical School Calendar">Calendar</a></li>
			<li><a href="https://med.uth.edu/az-index/" title="A-Z Index">A-Z</a></li>
			<li><a href="https://med.uth.edu/news/" title="News">News</a></li>
			<li><a href="https://www.uth.edu/index/campus-maps.htm" target="_blank" title="Campus Maps">Maps</a></li>
			<li id="insideUT"><a href="https://inside.uthouston.edu/" target="_blank" title="Inside UTHealth">Intranet</a></li>
			<li>
				<form name="gs" id="gs" action="https://med.uth.edu/search" method="get">
					<fieldset>
						<label for="q" class="visuallyhidden">Enter a Search Term</label>
						<input class="txt" type="text" onclick="this.value='';" onfocus="this.select()" onblur="this.value=!this.value?'Enter a search term…':this.value;" value="Enter a search term…" id="q" name="q" size="30" maxlength="255" />
						<input type="submit" name="btnG" class="button" value="Search" />  
						<input type="hidden" name="entqr" value="0" />
						<input type="hidden" name="output" value="xml_no_dtd" />
						<input type="hidden" name="sort" value="date:D:L:d1" />
						<input type="hidden" name="client" value="medfrontend" />
						<input type="hidden" name="ud" value="1" />
						<input type="hidden" name="oe" value="UTF-8" />
						<input type="hidden" name="ie" value="UTF-8" />
						<input type="hidden" name="proxystylesheet" value="medfrontend" />
						<input type="hidden" name="proxyreload" value="1" />
						<input type="hidden" name="site" value="medcollection" />
					</fieldset>
				</form>
			</li>
		</ul>
	
	</section>
						
</header>
<?php /*
<section class="weatheralert">
	<p><span class="alert-sign">&#9888;</span> All Offices will be closed Friday, Jan 24, 2014, due to inclement weather. Please visit <a href="https://www.uthoustonemergency.org/">https://www.uthoustonemergency.org/</a> for more updates!</p>
</section>
*/ ?>

