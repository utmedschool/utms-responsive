<footer id="footer">
		
	<div class="container" id="preFooter">
	
		<div class="row">
		
			<section class="fourcol" id="utcontactInfo"	>
			
				<h1><a href="<?php echo network_home_url(); ?>"><img src="<?php bloginfo('template_directory'); ?>/img/logo/utms-footer.png" alt="UTHealth Medical School"><span class="sr-only">UTHealth Medical School</span></a></h1>
				
				<h2><a href="http://med.uth.edu/ooc" title="Office of Communications"><img src="<?php bloginfo('template_directory'); ?>/img/logo/ooc-logo.png" alt="Website by Office of Communications"><span class="sr-only">UTHealth Medical School Office of Communications</span></a></h2>

				<address>
					<span class="bold">Contact / Feedback</span><br />
					6431 Fannin<br />
					Houston, Texas 77030<br />
					phone 713.500.4472<br />
					<?php //fax 713.500.5533 ?>
				</address>
				
			</section>
			
			<section class="twocol" id="utmsLinks">
				
				<h2><a href="<?php echo network_home_url(); ?>" title="UTHealth Medical School Home">UTHealth Medical School</a></h2>
				
				<nav>
					<ul>
						<li><a href="https://med.uth.edu/about/" title="About UTMed School">About us</a></li>
						<li><a href="https://med.uth.edu/education/" title="About Education in UTMed School">Education</a></li>
						<li><a href="https://med.uth.edu/research/" title="About Research in UTMed School">Research</a></li>
						<li><a href="https://med.uth.edu/patientcare/" title="About Patient Care in UTMed School">Patient Care</a></li>
						<li><a href="https://med.uth.edu/departments/">Departments</a></li>
						<li><a href="https://www.uth.edu/hr/careers.htm" title="Human Resources/Careers">Careers</a></li>
						<li><a href="https://www.facebook.com/utmedschool" title="UTMedschool Facebook Page" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/img/icons/footer-facebook.png" alt="Facebook" class="footericon" />Facebook</a></li>
						<li><a href="https://twitter.com/#!/utmedschool" title="UTMedschool Twitter" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/img/icons/footer-twitter.png" alt="Twitter" class="footericon" />Twitter</a></li>
					</ul>
				</nav>
				
			</section>
			
			<section class="twocol" id="utpLinks">
			
				<h2><a href="http://www.utphysicians.com" title="UT Physicians" target="_blank" title="UT Physicians Home">UT Physicians</a></h2>
				<nav>
					<ul>
						<li><a href="https://www.utphysicians.com/providers/" target="_blank" title="UTPhysicians Physician Directory">Physician Directory</a></li>
						<li><a href="https://www.utphysicians.com/about" target="_blank" title="About UTPhysicians">About UT Physicians</a></li>
						<li><a href="https://www.utphysicians.com/events/" target="_blank" title="Events at UTPhysicians">Events @ UTP</a></li>
						<li><a href="https://www.utphysicians.com/locations/" target="_blank" title="Clinics and Locations">Clinics and Locations</a></li>
						<li><a href="https://www.utphysicians.com/clinics/" target="_blank" title="Clinics by Specialty">Clinics by Specialty</a></li>

						<li><a href="https://www.utphysicians.com/patients/about-your-visit/" target="_blank" title="About Your Visit">About Your Visit</a></li>
						<li><a href="http://www.facebook.com/UTPhysicians" target="_blank" title="UTPhysicians Facebook Page"><img src="<?php bloginfo('template_directory'); ?>/img/icons/footer-facebook.png" alt="Facebook" class="footericon" />UTP Facebook</a></li>
						<li><a href="https://plus.google.com/116075356286933669737/posts" target="_blank" title="UTPhysicians Google+ Page"><img src="<?php bloginfo('template_directory'); ?>/img/icons/footer-google.png" alt="Facebook" class="footericon" />UTP Google+</a></li>
						<li><a href="https://twitter.com/#!/UTPhysicians" target="_blank" title="UTPhysicians on Twitter"><img src="<?php bloginfo('template_directory'); ?>/img/icons/footer-twitter.png" alt="Facebook" class="footericon" />UTP Twitter</a></li>
					</ul>
				</nav>
				
			</section>
			
			<section class="fourcol last" id="quickLinks">
			
				<h2><a href="#">Quick Links</a></h2>
				
				<nav>
					<?php 
					wp_nav_menu(array(
						'menu' => 'quick-links', 
						'container' => 'ul',
						'menu_id' => '', 
						'menu_class' => 'menu-footer',
					)); ?>
				</nav>
			
			</section>
		
		</div>
	
	</div><!--ending #preFooter -->
	
	<section id="credits">
		<div id="creditsHolder">
			<ul id="credit-column1">
				<li><span class="supercopyright">&copy; Copyright UTHealth <?php echo date('Y'); ?>. All Rights Reserved.</span></li>
				<li><a href="https://www.uthealthemergency.org/" title="Emergency Information" target="_blank">Emergency Information</a></li>
				<li><a href="http://www.governor.state.tx.us/homeland" title="Texas Homeland Security" target="_blank">Texas Homeland Security</a></li>
			</ul>
			
			<ul id="credit-column2">
				<li><a href="http://www.utsystem.edu/" title="UT System" target="_blank">UT System</a></li>
				<li><a href="http://www.texas.gov/en/Pages/default.aspx" title="State of Texas" target="_blank">State of Texas</a></li>
				<li><a href="https://www.tsl.state.tx.us/landing/trail.html" title="Texas Government Web Site Archive TRAIL" target="_blank">Statewide Search</a></li>
			</ul>
			
			<ul id="credit-column3">
				<li><a href="https://www.uth.edu/index/policies.htm" title="Website Policies" target="_blank">Website Policies</a></li>
				<li><a href="https://www.uth.edu/index/file-viewing-information.htm" title="Need help opening PDF or DOC files?" target="_blank">Need help opening PDF or DOC files?</a></li>
				<li><a href="https://med.uth.edu/report-website-error/" title="Report Website Errors to the Site Publisher" target="_blank">Errors? Report to the Site Publisher</a></li>
			</ul>
		</div>			
	</section><!--ending #credits-->
				
</footer>
	
	<?php if (!is_home()){?>			
	<script type="text/javascript" src="//use.typekit.net/zqa0smx.js"></script>
	<script type="text/javascript">try{Typekit.load();}catch(e){}</script>
	<?php } ?>
	
	<?php if (is_home()) {?>
		<script src="<?php bloginfo('template_directory'); ?>/js/jquery.flexslider.js"></script>
	<?php } elseif (is_page()) {?>
		<script src="<?php bloginfo('template_directory'); ?>/js/jquery.flexslider.js"></script>
	<?php } elseif (is_category('news') || is_single() ) { ?>
	<?php /*<script src="<?php bloginfo('template_directory'); ?>/js/twitterjs-2.0.0.js"></script>*/ ?>
	<script src="<?php bloginfo('template_directory'); ?>/js/jquery.fancybox.pack.js"></script>
	<?php
		wp_enqueue_script('jquery-ui-core', $in_footer);
		wp_enqueue_script('jquery-ui-widget', $in_footer);
		wp_enqueue_script('jquery-ui-tabs', $in_footer);
	} ?>


<?php wp_footer(); ?>

<?php if (is_home() ){?>
	<script src="<?php bloginfo('template_directory'); ?>/js/functions-home.js"></script>
 <?php	} elseif (is_page()) { ?>	
	<script src="<?php bloginfo('template_directory'); ?>/js/functions-page.js"></script>
<?php	} elseif (is_category('news') || is_single() ) { ?>
	<script src="<?php bloginfo('template_directory'); ?>/js/functions-news.js"></script>
<?php } ?>
	
</body>

</html>
