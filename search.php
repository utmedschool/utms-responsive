<?php get_header(); ?>

<div class="container" id="logoSection">
			
	<div id="mainLogo" class="row noSlider">
	
		<section class="sevencol">
			<h1 id="pageTitle">Search Results</h1>
		</section>
		
		<section class="fivecol last">
			<h2 id="utmsasSecondary"><a href="<?php echo site_url(); ?>" title="University of Texas Medical School at Houston" target="_parent"><img src="<?php bloginfo('template_directory'); ?>/img/logo/medschool-logo-large-current.png" alt="University of Texas Medical School at Houston" /></a></h2>
		</section>
		
	</div><!--end row-->
	
</div><!-- ending #logoSection-->
		
<?php include('includes/mission-nav.php'); ?>

<div class="row" id="mainContent">

	<section id="mainArticle" class="ninecol">
	
	<?php if (have_posts()) : ?>
	
	<?php 
		$allsearch = &new WP_Query("s=$s&showposts=-1"); 
		$key = wp_specialchars($s, 1);
		$count = $allsearch->post_count; _e(''); _e('<span class="search-terms">');
	?> 

	<h1><?php echo $count; ?> results for <?php echo '&ldquo;'.get_search_query().'&rdquo;'; ?></h1>
	
	<?php wp_reset_query(); ?>
	
	<?php
		global $wp_query;
		$big = 99999999;
		echo paginate_links(array(
		'base' => str_replace($big, '%#%', get_pagenum_link($big)),
		'format' => '?paged=%#%',
		'total' => $wp_query->max_num_pages,
		'current' => max(1, get_query_var('paged')),
		'show_all' => false,
		'end_size' => 2,
		'mid_size' => 3,
		'prev_next' => true,
		'prev_text' => 'Prev',
		'next_text' => 'Next',
		'type' => 'list'
		));
	?>

	<?php while (have_posts()) : the_post(); ?>
		
		<article class="clear" id="post-<?php the_ID(); ?>">

			<h2><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h2>

			<div id="searchResult" class="entry">

				<?php // the_excerpt(); 
				// http://wp-snippets.com/highlight-search-result/
				?>
				
				<?php
					// Replace the_exerpt() with:
					$excerpt = strip_tags(get_the_excerpt());
					$keys = explode(" ",$s);
					$excerpt = preg_replace('/('.implode('|', $keys) .')/iu', '<strong class="search-excerpt">\0</strong>', $excerpt);
					echo '<p>'.$excerpt.' <a href="'.the_permalink().'">read more</a></p>'; 
				?>
				
				<p class="button"><a class="css3button" href="<?php the_permalink(); ?>" title="More about <?php the_title(); ?>">Open Full...</a></p>
				
				<hr />

			</div>

		</article>

	<?php endwhile; ?>

	<?php else : ?>

		<h2>No posts found.</h2>

	<?php endif; ?>

	</section> <!-- ending mainArticle -->
	
	<section class="threecol last">
		
		<?php get_sidebar(); ?>
		
		<?php include (TEMPLATEPATH . '/includes/righticon-buttons.php'); ?>
		
	</section>

</div> <!--ending #mainContent-->

<?php get_footer(); ?>