<?php get_header(); ?>

<div class="container" id="logoSection">
	<div id="mainLogo" class="row noSlider">
		<section class="sevencol">
			<h1 id="pageTitle">News</h1>
		</section>
		<section class="fivecol last">
			<h2 id="utmsasSecondary"><a href="<?php echo site_url(); ?>" title="University of Texas Medical School at Houston" target="_parent"><img src="<?php bloginfo('template_directory'); ?>/img/logo/medschool-logo-large-current.png" alt="University of Texas Medical School at Houston" /></a></h2>
		</section>
	</div><!--end row-->
</div><!-- ending #logoSection-->

<div id="above-the-fold" class="container">
	<div class="row">
		<div class="fivecol">
			<p class="section-headline"><strong class="darkblue">recent</strong> headlines  //  <a href="<?php echo get_month_link('', ''); ?>" title="All news for this month">archives</a></p>
						
			<div id="main-news-story">

    <?php /*<h1 class="big-header"><a href="https://vimeo.com/136719343">See all the action in the 2015 Medical School Retreat</a></h1>
    <div class="video-frame"><iframe src="https://player.vimeo.com/video/136719343?autoplay=0&loop=0" frameborder="0" width="547" height="303" allowfullscreen=""></iframe></div>
    <p><hr class="news-separator"></p> */ ?>

				<?php $recent = new WP_Query('category_name=homepage-primary&posts_per_page=1'); while ($recent->have_posts()) : $recent->the_post(); ?>
				<h1 class="big-header"><a href="<?php the_permalink(); ?>" title="Read more about <?php the_title(); ?>"><?php the_title(); ?></a></h1>
				<?php the_post_thumbnail( 'newspage-main' ); ?>
				<?php echo the_excerpt(); ?>
				<p class="readmore"><a href="<?php the_permalink();?>" title="read more about <?php the_title();?>">read more &raquo;</a></p>
				<?php endwhile; ?>
				<?php wp_reset_postdata(); ?>
			</div>
			
			<ul class="news-list">
				<?php $recent2 = new WP_Query('category_name=news&showposts=4&cat=-475, -490, -508, -350, -570'); while ($recent2->have_posts()) : $recent2->the_post(); ?>
				<?php // excludes news from the primary category ?>
				<li>
					<h2><a href="<?php the_permalink(); ?>" title="Read more about <?php the_title(); ?>"><?php the_title(); ?></a></h2>
					<?php the_post_thumbnail( 'newspage-main' ); ?>
					<?php echo the_excerpt(); ?>
					<p class="readmore"><a href="<?php the_permalink();?>" title="read more about <?php the_title();?>">read more &raquo;</a></p>
				</li>
				<?php endwhile; ?>
				<?php wp_reset_postdata(); ?>
			</ul>
			
			<div id="scoop-subscription">
				<p><span class="scoop-logo">Scoop</span> is a weekly electronic newsletter providing timely information to the Medical School. To receive scoop right in your inbox weekly,</p>
				<?php gravity_form(40, false, false, false, '', false); ?>
				<p><a href="http://med.uth.tmc.edu/comm/Scoop/archive/2015.html" title="View Scoop Archives">view previous archives</a></p>
			</div><!--end scoop-subscription-->	
			
		</div><!-- ending fivecol-->
		
		<div class="sevencol last">
			<p class="section-headline">wild<strong class="darkblue">art</strong> //  <a href="<?php echo get_category_link(479); ?>" title="Wild Art Archives">archives</a></p>
			<section>
				
				<div class="wild-art-section">
				
				<div id="wild-art-feature">
				<?php $recent3 = new WP_Query('category_name=wildart&showposts=1'); while ($recent3->have_posts()) : $recent3->the_post(); ?>
				<?php 
					$post_id = get_the_ID();
					$images = get_children( 
								array( 	'post_parent' => $post_id, 
										'post_type' => 'attachment', 
										'post_mime_type' => 'image', 
										'exclude' => get_post_thumbnail_id($post_id)) 
									); 
					$total_images = count($images);
				?>
						<h1 class="big-header white"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"?><?php the_title(); ?></a></h1>
						
						<?php if (($total_images != 0) && ($total_images != 1) ) {?>
							<a href="<?php the_permalink(); ?>" class="gallery-flag"><?php echo $total_images; ?> more images</a>
						<?php } elseif ($total_images == 1) { ?>
							<a href="<?php the_permalink(); ?>" class="gallery-flag"><?php echo $total_images; ?> more image</a>
						<?php } ?>
						
						<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_post_thumbnail( 'wildart-full' ); ?></a>
						<?php the_excerpt(); ?>
						<p class="wild-art-byline">&mdash; <?php echo (get_post_meta($post->ID, 'photo-byline', TRUE));?></p>
				<?php endwhile; ?>
				<?php wp_reset_postdata(); ?>
				</div>
			
				<p class="section-headline">more wild<strong>art</strong></p>
				
				<?php $recent4 = new WP_Query('category_name=wildart&showposts=4&offset=1'); while ($recent4->have_posts()) : $recent4->the_post(); ?>
						<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail( 'wildart-thumb' ); ?></a>
				<?php endwhile; ?>
				<?php wp_reset_postdata(); ?>
				
				</div>
			</section>
			
			<p class="section-headline">news<strong class="darkblue"> of </strong>note // <a href="<?php echo get_post_type_archive_link( 'newsofnote' ); ?>">archives</a></p>
			<div id="news-of-note">
				<?php $today = getdate(); ?>
				
				<?php $recent5 = new WP_Query( 'post_type=newsofnote&showposts=1');
				if ($recent5-> have_posts()) : while ($recent5->have_posts()) : $recent5->the_post(); ?>
				
				<h1><?php the_title(); ?> <small><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" target="_blank">view email version</a></small></h1>
				<p><small>Some publications may require readers to register for free.</small></p>
				<ul class="news-of-note-list">
				<?php // Show news of notes from the latest day ?>
				<?php $newsofnotearray = get_post_meta($post->ID, '_details', true);
	            foreach ( $newsofnotearray as $item) {
                    	$title = $item['_title'];
                    	$excerpt = $item['_excerpt'];
                    	$url = $item['_url']; ?>
						<li>
							<h1><a href="<?php echo $url;?>" title="Read full article on <?php echo $title; ?>" target="_blank"><?php echo $title; ?></a></h1>
							<p><?php echo $excerpt; ?></p>
						</li>
				<?php } //end foreach loop for bundle?>
				</ul>
				<hr />
				<div id="news-of-note-subscription">
					<p>To receive <a class="darkblue bold" href="<?php the_permalink(); ?>">News of Note</a> daily in your inbox, please subscribe</p>
					<?php endwhile; endif; ?>
					<?php wp_reset_postdata(); ?>
					<?php gravity_form(42, false, false, false, '', false); ?>
				</div>
			</div><!--ending news-of-note-->
		</div><!--ending sevencol-->
		
	</div>
</div><!-- end of Above the Fold -->

<div id="below-the-fold" class="container">
	<div class="row">
		<div class="fourcol">
			<?php 
				$recent6 = new WP_Query( 'category_name=memoriam&showposts=1' );while ($recent6->have_posts()) : $recent6->the_post(); 
			?>
				<div class="in-memoriam">
					<?php the_post_thumbnail('memoriam-img'); ?>
					<h2><a href="<?php the_permalink(); ?>" title="Read more about <?php the_title(); ?>"><?php the_title(); ?></a></h2>
					<?php the_excerpt(); ?>
					<button class="btn btn-primary btn-default"><a href="<?php the_permalink(); ?>" title="Read more about <?php the_title(); ?>">Read More</a></button>
				</div>
			<?php endwhile; ?>
			<?php wp_reset_postdata(); ?>
			
			
			<section id="eventsSection">
			<p class="section-headline">upcoming <strong class="darkblue">events</strong></p>
				<p class="css3button" style="margin-top:30px; margin-bottom:30px;">
					<a href="<?php echo tribe_get_events_link(); ?>" title="View the Events Calendar"><img src="<?php bloginfo('template_directory'); ?>/img/icons/calendar.png" alt="calendar" class="calendarinhomeButton" /><span class="calendarTxt">View the events calendar</span></a>
				</p>

				<ul class="eventListing">
				<?php 
				global $post;
				$all_events = tribe_get_events(array(
							'eventDisplay'=>'upcoming',
							'posts_per_page'=>6
						));
				foreach($all_events as $post) { setup_postdata($post); ?>
				<li>
				<a href="<?php the_permalink(); ?>" id="post-<?php the_ID(); ?>">
				<div class="eventDate">
					<span class="month"><?php echo tribe_get_start_date($post->ID, false, 'M'); ?></span>
					<span class="day"><?php echo tribe_get_start_date($post->ID, false, 'j'); ?></span>
				</div>
				
				<div class="eventDetails">
				 <?php 
				 $themeta = get_post_meta($post->ID, 'event-series', TRUE);
				 if ($themeta != '') { ?>
					<span class="event-series"><?php echo tribe_get_event_meta($post->ID, $meta = 'event-series'); ?>: </span>
					<?php } ?>
						<?php if (tribe_get_organizer()) { ?>
						<span class="event-organizer"><?php echo tribe_get_organizer(); ?> presents</span>
						<?php } ?>
				        <span class="event-title"><?php the_title(); ?></span>
				        	<?php if(tribe_get_venue()) : ?>
							<span class="event-venue">
								<?php echo tribe_get_venue(get_the_ID()); ?>
							, </span>
							<?php endif; ?>
						<span class="event-date">
						<?php if( !tribe_get_all_day() ) { ?>
							<?php echo tribe_get_start_date($post->ID, true, ' '); ?> <?php echo tribe_get_end_date( $post->ID, true, '–' );?><br /><?php echo tribe_get_start_date($post->ID, false, 'M j, Y'); ?>
						<?php } else { ?>
							All Day Event
						<?php } ?>
						</span>
					 </div></a>
					    </li>
					<?php } //endforeach ?>
				</ul>
			<?php wp_reset_query(); ?>
								
			</section><!--ending eventDetails -->
						
					<?php 
						$week = date('W');
						$year = date('Y');
						$recent7 = new WP_Query( 'category_name=utmost&showposts=1' );while ($recent7->have_posts()) : $recent7->the_post(); 
					?>
					<div class="utmost">
						<h1>UTMost</h1>
						<?php the_content(); ?>
					</div>
						<?php endwhile; ?>
						<?php wp_reset_postdata(); ?>
						
						
					<?php $spotlight=get_posts('category_name=spotlight'); 
					if ($spotlight) { ?>
						<div class="in-the-spotlight">
							<section>
							<h1>In the Spotlight</h1>
							<?php $recent8 = new WP_Query('category_name=spotlight&showposts=1'); while ($recent8->have_posts()) : $recent8->the_post(); ?>
							<?php the_post_thumbnail('spotlight-img'); ?>
							<h2><a href="<?php the_permalink(); ?>" title="Read more about <?php the_title(); ?>"><?php the_title(); ?></a></h2>
							<?php the_content(); ?>
							</section>
						</div>
						<?php endwhile; ?>
						<?php wp_reset_postdata(); ?>
					<?php } ?>
					

					<div class="tips4u">
						<section>
						<img src="<?php echo get_template_directory_uri(); ?>/img/news/tips4u-logo.png" title="Tips4u" alt="Tips4u">
						<?php $recenttips = new WP_Query('category_name=tips4u&showposts=1'); while ($recenttips->have_posts()) : $recenttips->the_post(); ?>
						<h1><?php the_title(); ?></h1>
						<?php the_content(); ?>
						<?php endwhile; ?>
						<?php wp_reset_postdata(); ?>
						</section>
					</div>


		</div>	<!--ending four col-->


		
		<div class="eightcol last">
		
			<div class="instagram">
			<p class="section-headline"><a href="http://instagram.com/utmedschool" target="_blank" title="UTMedschool Profile on Instagram"><strong class="darkblue">Follow</strong> utmedschool on &nbsp;&nbsp;<img src="<?php bloginfo('template_directory'); ?>/img/logo/instagram-logo.png" alt="UTMedschool in Instagram" /></a></p>
			<div id="instagram-holder" class="white45">
				<?php
					// Supply a user id and an access token
					$userid = "192655275";
					$accessToken = "192655275.ab103e5.3a8cb3a87b7e4d5c9221ffaf8e715292";
			
					// Gets our data
					function fetchData($url){
					     $ch = curl_init();
					     curl_setopt($ch, CURLOPT_URL, $url);
					     curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					     curl_setopt($ch, CURLOPT_TIMEOUT, 20);
					     $result = curl_exec($ch);
					     curl_close($ch); 
					     return $result;
					}
			
					// Pulls and parses data.
					$result = fetchData("https://api.instagram.com/v1/users/{$userid}/media/recent/?access_token={$accessToken}");
					$result = json_decode($result);
					$counter = 1;
					
					
						foreach ($result->data as $post)
						{
							if ($counter<=7){
							
							/* USING substr_replace to replace http with https */
							$instaurl = $post->images->standard_resolution->url;
							$instaurls = substr_replace($instaurl, $insert_string, 4, 0);
							$instathumburl = $post->images->thumbnail->url;
							$instathumburls = substr_replace($instathumburl, $insert_string, 4, 0);
							$instacaption = $post->caption->text;
							/*
							
							Testing for HTTPS URL
							//echo $instaurls;
							//echo '<a class="fancybox" rel="group" href="'.$post->images->standard_resolution->url.'"><img src="'.$post->images->thumbnail->url.'" alt="UTMedschool in Instagram"></a>';
							// Also adding a class of fancybox and group will show the images in a gallery format
							
							*/
							echo '<a class="fancybox" rel="group" href="'.$instaurls.'" title="'.$instacaption.'"><img src="'.$instathumburls.'" alt="UTMedschool in Instagram"></a>';
						}
					$counter++;
					}
				?>
			</div>
		</div>
		
		
			<p class="section-headline"><strong class="darkblue">twitter</strong> feeds</p>
			<div id="twitter">
				<a class="twitter-timeline" data-dnt="true" href="https://twitter.com/UTMedSchool"  data-widget-id="248888674706653185">Tweets by @UTMedSchool</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>

			</div><!--end twitter-->
			
			
		
		<div id="comments" class="news-tabs">
			   <ul class="news-tabs-nav tabs-clearfix">
			      <li><a href="#popular-stories">Popular Stories</a></li>
			      <li><a href="#recent-comments">Comments</a></li>
			      <li><a href="#all-tags">Tags</a></li>
			      <li><a href="#recent-stories">Recent News</a></li>
			   </ul>
			   <div id="popular-stories" class="news-tabs-panel">
				  <ol>
				  	<?php 
					$popularpost = new WP_Query( array( 
									'posts_per_page' => 10, 
									'meta_key' => 'utms_post_views_count', 
									'orderby' => 'meta_value_num', 
									'order' => 'DESC',
									'cat'	=> -490 ) );
					while ( $popularpost->have_posts() ) : $popularpost->the_post(); ?>
					<li><a href="<?php the_permalink(); ?>" title="Read more about <?php the_title(); ?>"><?php the_title(); ?></a></li>
					<?php endwhile; ?>
					<?php wp_reset_postdata(); ?>
				  </ol>
				</div>
			   <div id="recent-comments" class="news-tabs-panel">
			      	<ul class="comments-list">

					<?php 
						$comments = get_comments('number=15&amp;status=approve');
						$true_comment_count = 0;
						foreach($comments as $comment) :
						$comment_type = get_comment_type(); 
						if($comment_type == 'comment') { 
						$true_comment_count = $true_comment_count +1; 
						$comm_title = get_the_title($comment->comment_post_ID);
						$comm_link = get_comment_link($comment->comment_ID);
						$comm_comm_temp = get_comment($comment->comment_ID,ARRAY_A);
						$comm_content = $comm_comm_temp['comment_content'];
					?>
					<li>
					<?php echo get_avatar( $comment, '25' ); ?>
					<span class="footer_comm_author"><?php echo($comment->comment_author)?></span> on <a href="<?php echo($comm_link)?>" title="<?php comment_excerpt(); ?>"> <strong><?php echo $comm_title?> </strong></a>
					<br />
					<span class="comment-quote">&ldquo;</span>
						<?php echo $comm_content; ?>
					<span class="comment-quote">&rdquo;</span>
					
					</li> 
					<?php } ?>
					<?php if($true_comment_count == 5) {break;} ?>
					<?php endforeach;?>
					</ul>
					
			   </div>
			   <div id="all-tags" class="news-tabs-panel">
			      <div class="tagcloud">
			      	<?php wp_tag_cloud('smallest=7&largest=24&orderby=count&order=DESC'); ?>
				   </div>
			   </div>
			   <div id="recent-stories" class="news-tabs-panel">
				  <ol>
				  	<?php 
					$recent9 = new WP_Query( array( 
									'posts_per_page' => 10, 
									'orderby' => 'date', 
									'order' => 'DESC',
									'category_name' => 'news',
									'cat'			=>	-490 ) );
					while ( $recent9->have_posts() ) : $recent9->the_post(); ?>
					<li><a href="<?php the_permalink(); ?>" title="Read more about <?php the_title(); ?>"><?php the_title(); ?></a></li>
					<?php endwhile; ?>
				  </ol>
				</div>
			<!-ending recent stories-->
			</div><!--ending tabs-->


		</div><!--ending the eightcol-->		
		
	</div><!--ending row -->
	
<?php get_footer(); ?>
