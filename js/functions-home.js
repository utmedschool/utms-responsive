//jQuery(document).ready(function(){
jQuery(window).bind("load", function() { // Only after binding the load function, the slider functions automatically
    
	jQuery('.flexslider').flexslider({
    	animation: "fade",              
		slideshow: true,                
		slideshowSpeed: 9000,        
		animationDuration: 500,         
		directionNav: false,            
		controlNav: true,               
		keyboardNav: true,          
		mousewheel: false, 
		prevText: "&larr; Previous",           
		nextText: "Next &rarr;",
		pausePlay: false,               
		pauseText: 'Pause',            
		playText: 'Play',                         
		randomize: true,               
		animationLoop: true,            
		pauseOnAction: true,          
		pauseOnHover: false,
		start: function(slider) {
                        slider.removeClass('loadingif');
                }            
		//controlsContainer: "#flexcontainer",          
		//manualControls: "#tabs-nav li a"             //Selector: Declare custom control navigation. Example would be ".flex-control-nav li" or "#tabs-nav li img", etc. The number of elements in your controlNav should match the number of slides/tabs.
    });
	
});
