<!doctype html>
<html class="no-js" <?php language_attributes(); ?>>

<head>

	<meta charset="<?php bloginfo('charset'); ?>" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	
	<?php if (is_search()) { ?>
	<meta name="robots" content="noindex, nofollow" /> 
	<?php } ?>
	
	<meta property="twitter:account_id" content="18809763" />

	<title>
		   <?php
		      if (function_exists('is_tag') && is_tag()) {
		         single_tag_title("Tag Archive for &quot;"); echo '&quot; - '; }
		      elseif (is_archive()) {
		         wp_title(''); echo ' - '; }
		      elseif (is_search()) {
		         echo 'Search for &quot;'.wp_specialchars($s).'&quot; - '; }
		      elseif (!(is_404()) && (is_single()) || (is_page())) {
		         wp_title(''); echo ' - '; }
		      elseif (is_404()) {
		         echo 'Not Found - '; }
		      if (is_home()) {
		         bloginfo('name'); echo ' - Home '; }
		      else {
		          bloginfo('name'); }
		      if ($paged>1) {
		         echo ' - page '. $paged; }
		   ?>
	</title>
		   
	<?php if (is_home()) { ?>
		<meta name="description" content="<?php bloginfo('description'); ?>">
	<?php } ?>
	
	<meta name="google-site-verification" content="TimdGzuLl1Yw4Dq45FMXkoWoVgJiZXFGs9itQCqhjXg">	
	<meta name="author" content="Office of Communication, Medical School">
	<meta name="Copyright" content="Copyright UTHealth Medical School <?php echo date('Y'); ?>. All Rights Reserved.">

	<!--  Dublin Core Metadata : http://dublincore.org/ -->
	<meta name="DC.title" content="UT Medical School at Houston Website">
	<meta name="DC.subject" content="Education, Research, Patient Care">
	<meta name="DC.creator" content="UTHealth">

	<meta name="viewport" content="width=device-width, maximum-scale=1.0" />
	
	<link rel="shortcut icon" href="<?php bloginfo('template_directory'); ?>/img/common/favicon.ico">		 
	<?php /*<link rel="apple-touch-icon" href="<?php bloginfo('template_directory'); ?>/universal/img/apple-touch-icon.png"> */ ?>
	<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>">
	<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/flexslider.css" type="text/css">
	
	<script type="text/javascript" src="//use.typekit.net/zqa0smx.js"></script>
	<script type="text/javascript">try{Typekit.load();}catch(e){}</script>
	
	<script src="<?php bloginfo('template_directory'); ?>/js/modernizr-2.0.6.js"></script>
		
	
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
	<?php if ( is_singular() ) wp_enqueue_script( 'comment-reply' ); ?>
	<?php wp_head(); ?>
	<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/css3-mediaqueries.js"></script>
	<!--[if lt IE 9]>
		<script src="https://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
	<![endif]-->
	
	<!--[if lte IE 9]><link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/ie.css" type="text/css" media="screen" /><![endif]-->

	<script type="text/javascript">
	window.addEventListener("load",function() {
	  setTimeout(function(){
	    window.scrollTo(0, 1);
	  }, 0);
	});
	</script>
	
	<script type="text/javascript">
	
	  var _gaq = _gaq || [];
	  _gaq.push(['_setAccount', 'UA-27035114-1']);
	  _gaq.push(['_setDomainName', 'uth.edu']);
	  _gaq.push(['_trackPageview']);
	
	  (function() {
	    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();
	
	</script>

</head>

<body <?php body_class(); ?>>
	
	<?php // For accessibility and compliance to Section 508 ?>
	
	<div id="skip">
		<a href="#mainContent" accesskey="m">Skip to Main Content <small>(accesskey m)</small></a> <a href="#linksResources" accesskey="n">Skip to Navigation <small>(accesskey n)</small></a>
	</div> <!-- END #skip -->
	
	<?php echo open_ext("https://med.uth.edu/utms-universal/utms-superinclude-topnav.php"); ?>

