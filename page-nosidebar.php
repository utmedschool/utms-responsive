<?php 
/*
Template Name: Default no sidebar
*/

get_header(); ?>

<div class="container" id="logoSection">
			
	<div id="mainLogo" class="row noSlider">
	
		<section class="sevencol">
			<h1 id="pageTitle"><?php the_title(); ?></h1>
		</section>
		
		<section class="fivecol last">
			<h2 id="utmsasSecondary"><a href="<?php echo site_url(); ?>" title="University of Texas Medical School at Houston" target="_parent"><img src="<?php bloginfo('template_directory'); ?>/img/logo/medschool-logo-large-current.png" alt="University of Texas Medical School at Houston" /></a></h2>
		</section>
		
	</div><!--end row-->
	
</div><!-- ending #logoSection-->

<?php include('includes/mission-nav.php'); ?>

<div class="container" id="mainContent">

	<div class="row">

		<section class="threecol">
			
			<?php include('includes/left-sidebar.php'); ?>
			
		</section>
		
		<section id="mainArticle" class="ninecol last">
	
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
				
			<article class="post" id="post-<?php the_ID(); ?>">
	
				<?php // include (TEMPLATEPATH . '/_/inc/meta.php' ); ?>
	
				<div class="entry">
	
					<?php the_content(); ?>
	
					<?php wp_link_pages(array('before' => 'Pages: ', 'next_or_number' => 'number')); ?>
	
				</div>
	
				<?php edit_post_link('Edit this entry.', '<p>', '</p>'); ?>
	
			</article>
			
			<?php // comments_template(); ?>
	
			<?php endwhile; endif; ?>
			
		</section> <!-- ending mainArticle -->
	
	
	</div>

</div> <!--ending #mainContent-->

<?php get_footer(); ?>
