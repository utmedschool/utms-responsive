<?php
/*
Template Name: News Archive
*/?>

<?php get_header(); ?>

<div class="container" id="logoSection">
			
	<div id="mainLogo" class="row noSlider">
		<?php
			$currentmonthname = date('F');
			$currentmonth = date('m');
			$currentyear = date('Y');
		?>
		<section class="sevencol">
			<h1 id="pageTitle"><?php echo $currentmonthname; ?> News</h1>
		</section>
		
		<section class="fivecol last">
			<h2 id="utmsasSecondary"><a href="<?php echo site_url(); ?>" title="University of Texas Medical School at Houston" target="_parent"><img src="<?php bloginfo('template_directory'); ?>/img/logo/medschool-logo-large-current.png" alt="University of Texas Medical School at Houston" /></a></h2>
		</section>
		
	</div><!--end row-->
	
</div><!-- ending #logoSection-->

<div  id="above-the-fold" class="container">
	<div class="row">
		<div class="sevencol">
			
			<p class="section-headline"><strong class="darkblue">headlines</strong> // <?php echo $currentmonthname; ?>, <?php echo $currentyear; ?></p>
			
			<select name="archive-dropdown" onchange="document.location.href=this.options[this.selectedIndex].value;">
  <option value=""><?php echo esc_attr( __( 'Select Month' ) ); ?></option> 
  <?php wp_get_archives( 'type=monthly&format=option&show_post_count=1' ); ?>
</select>
			
			<ul class="news-list newsarchive">
				<?php $recent = new WP_Query( array( 
								'category_name'		=> 'news',
								'category__not_in' => array( 490, 478 ),
								'monthnum'			=> $currentmonth,
								'year'				=> $currentyear
								) );
								while ($recent->have_posts()) : $recent->the_post(); ?>
				<li>
					<h2><a href="<?php the_permalink(); ?>" title="Read more about <?php the_title(); ?>"><?php the_title(); ?></a></h2>
					<p class="tags"><?php the_tags( '', '', '' ); ?></p>
				</li>
				<?php endwhile; ?>
				<?php wp_reset_postdata(); ?>
			</ul>
			
		</div><!-- ending fivecol-->
		
		<div class="fivecol last">
			<p class="section-headline">wild<strong class="darkblue">art</strong> archives for <?php echo $currentmonthname; ?>, <?php echo $currentyear; ?></p>
			<section>
				<ul class="wild-art-section wildartarchive">
				<?php $recent = new WP_Query( array( 
								'category_name'		=> 'wildart',
								'monthnum'			=> $currentmonth,
								'year'				=> $currentyear
								) );
				while ($recent->have_posts()) : $recent->the_post(); ?>
					<li>
						<h2><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></h2>
						<?php
						$args = array(
							'order'          => 'ASC',
							'post_type'      => 'attachment',
							'post_parent'    => $post->ID,
							'post_mime_type' => 'image',
							'post_status'    => null,
							'numberposts'    => 1,
						);
						$attachments = get_posts($args);
						if ($attachments) {
							foreach ($attachments as $attachment) {
								//echo apply_filters('post_title', $attachment->post_title);
								echo wp_get_attachment_image($attachment->ID, 'thumbnail', false, false);
							}
						}
						?>
					</a></li>
				<?php endwhile; ?>
				<?php wp_reset_postdata(); ?>
				
				</ul>
				
			</section>
			
			<p class="section-headline">news<strong class="darkblue"> of </strong>note</p>
			<div id="news-of-note">
				<?php $today = getdate(); ?>
				<h1>Recent News of Note</h1>
				<ul class="news-of-note-list">
				<?php $recent = new WP_Query( 'category_name=newsofnote&showposts=10');while ($recent->have_posts()) : $recent->the_post(); ?>
				<?php // Only shows news of note from today ?>
					<li>
						<a href="<?php $link = get_post_meta($post->ID, 'direct-external-link', TRUE); echo $link;?>" title="Read full article on <?php the_title(); ?>" target="_blank"><?php the_title(); ?></a>
					</li>
				<?php endwhile; ?>
				<?php wp_reset_postdata(); ?>
				</ul>
			</div><!--ending news-of-note-->
		</div><!--ending sevencol-->
		
	</div>
</div><!-- end of Above the Fold -->

<?php get_footer(); ?>
