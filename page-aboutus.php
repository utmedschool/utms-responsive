<?php 
/*
Template Name: About Us
*/

get_header(); ?>

<div class="container" id="logoSection">
			
	<div id="mainLogo" class="row">
	
		<section class="sevencol">
			<h1 id="pageTitle"><?php the_title(); ?></h1>
		</section>
		
		<section class="fivecol last">
			<h2 id="utmsasSecondary"><a href="<?php echo site_url(); ?>" title="University of Texas Medical School at Houston" target="_parent"><img src="<?php bloginfo('template_directory'); ?>/img/logo/medschool-logo-large-current.png" alt="University of Texas Medical School at Houston" /></a></h2>
		</section>
		
	</div><!--end row-->
	
</div><!-- ending #logoSection-->
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<div id="rslider" class="container">
	<div class="row">
		<div id="flexcontainer" class="twelvecol last">
			<div class="flexslider loadingif">
				<ul class="slides">
					<?php 
						$featureargs = array(
						'post_type' => 'featureimage',
						'posts_per_page' => 5,
						'feature_category'	=> 'aboutus'
						);
						$featurequery = new WP_Query( $featureargs );
						if ( $featurequery->have_posts() ) : while ( $featurequery->have_posts() ) : $featurequery->the_post(); 
							
							$cf = get_post_custom($post->ID);
							$directlink = $cf['_info_link_url'][0];	
							$pagelink = $cf['_info_page_select'][0];	
							$postlink = $cf['_info_post_select'][0];
							
							if (!empty($directlink)){
								$finalurl = get_permalink($directlink);
							} elseif ( (empty($directlink)) && (!empty($pagelink)) ) {
								$finalurl = get_permalink($pagelink);
							} elseif ( (empty($directlink)) && (empty($pagelink)) &&  (!empty($postlink)) ) {
								$finalurl = get_permalink($postlink);
							} else {
								$finalurl = '#';
							}
					?>
							<li><a href="<?php echo $finalurl; ?>"><?php the_post_thumbnail(); ?></a></li>
					<?php endwhile; else: ?>
							<li><a><img src="<?php bloginfo('template_directory');?>/img/slider-images/ms-building-ustxflag.jpg" alt="Medical School Building External Shot"></a></li>
					<?php endif;
						wp_reset_query();
					?>
					
				</ul>
			</div><!--ends flexslider-->
		</div><!--ends flexcontainer-->
	</div><!--ends .row-->
</div><!--ends rslider.container-->

<?php include('includes/mission-nav.php'); ?>

<div class="container" id="mainContent">
	
	<div class="row">
	
		<section class="threecol">
			<?php include('includes/left-sidebar.php'); ?>
		</section>
	
		<section id="mainArticle" class="sixcol">
			
			
				<article <?php post_class() ?> id="post-<?php the_ID(); ?>">
				<p><?php edit_post_link('Edit this entry',''); ?></p>
				<h1><?php the_title(); ?></h1>
	       		<?php the_content();?>
				</article>
			
			<?php endwhile; endif; ?>
		</section><!-- ending #contentDiv -->
		
		<aside class="threecol last">		
			<?php get_sidebar(); ?>
			<?php include (TEMPLATEPATH . '/includes/righticon-buttons.php'); ?>
		</aside>
		
	
	</div>
	
</div><!--ending #mainContent-->
	
<?php get_footer(); ?>
