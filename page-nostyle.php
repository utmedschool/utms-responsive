<?php 
/*
Template Name: Plain Page
*/
?>
<!doctype html>
<html class="no-js">

<head id="www-med.uth-edu" data-template-set="uthealthms-theme" profile="http://gmpg.org/xfn/11">

	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<title><?php echo the_title(); ?></title>
	<meta name="google-site-verification" content="TimdGzuLl1Yw4Dq45FMXkoWoVgJiZXFGs9itQCqhjXg">	
	<meta name="author" content="Office of Communications, suBi">
	<meta name="Copyright" content="Copyright UTHealth Med School 2012. All Rights Reserved.">

	<link rel="shortcut icon" href="<?php bloginfo('template_directory'); ?>/img/common/favicon.ico">		 
	<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>">
	<script src="<?php bloginfo('template_directory'); ?>/js/modernizr-2.0.6.js"></script>
	<?php wp_head(); ?>
	
</head>

<body id="plain-page" <?php body_class(); ?>>
	
	<?php // For accessibility and compliance to Section 508 ?>
	
	<div id="skip">
		<a href="#mainContent" accesskey="m">Skip to Main Content <small>(accesskey m)</small></a> <a href="#linksResources" accesskey="n">Skip to Navigation <small>(accesskey n)</small></a>
	</div> <!-- END #skip -->
	
	<div class="row" id="mainContent">

		<section id="mainArticle" class="twelvecol last">
				
			<article class="post" id="post-<?php the_ID(); ?>">
	
				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
				
				<h1><?php echo the_title(); ?></h1>
				  <?php the_content('read more'); ?>
				  <?php endwhile; endif; ?>
	
			</article>
					
		</section> <!-- ending mainArticle -->

	</div> <!--ending #mainContent-->
	
	<?php wp_footer(); ?>
	
</body>
</html>