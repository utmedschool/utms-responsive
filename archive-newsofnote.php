<?php get_header(); ?>

<div class="container" id="logoSection">
			
	<div id="mainLogo" class="row noSlider">
	<?php if (have_posts()) : ?>
		
		<section class="sevencol">
			<h1 id="pageTitle">News of Note Archives</h1>
		</section>
		
		<section class="fivecol last">
			<h2 id="utmsasSecondary"><a href="<?php echo site_url(); ?>" title="University of Texas Medical School at Houston" target="_parent"><img src="<?php bloginfo('template_directory'); ?>/img/logo/medschool-logo-large-current.png" alt="University of Texas Medical School at Houston" /></a></h2>
		</section>
		
	</div><!--end row-->
	
</div><!-- ending #logoSection-->

<?php include('includes/mission-nav.php'); ?>

<div id="above-the-fold" class="container">
	<div class="row">
		<div class="sevencol">
			
					<p class="section-headline">Archives available from September 20, 2013 and later only</p>
					
					<?php
						global $wp_query;
						$big = 99999999;
						echo paginate_links(array(
						'base' => str_replace($big, '%#%', get_pagenum_link($big)),
						'format' => '?paged=%#%',
						'total' => $wp_query->max_num_pages,
						'current' => max(1, get_query_var('paged')),
						'show_all' => false,
						'end_size' => 2,
						'mid_size' => 3,
						'prev_next' => true,
						'prev_text' => 'Prev',
						'next_text' => 'Next',
						'type' => 'list'
						));
					?>
					
					<ul class="news-list newsarchive">
					
					<?php while (have_posts()) : the_post(); ?>
							
								<li <?php post_class(); ?>>
									<h2><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" target="_blank">News of Note for <?php the_title(); ?></a></h2>
								</li>
		
					<?php endwhile; ?>
					
					</ul>
					
					<?php
						global $wp_query;
						$big = 99999999;
						echo paginate_links(array(
						'base' => str_replace($big, '%#%', get_pagenum_link($big)),
						'format' => '?paged=%#%',
						'total' => $wp_query->max_num_pages,
						'current' => max(1, get_query_var('paged')),
						'show_all' => false,
						'end_size' => 2,
						'mid_size' => 3,
						'prev_next' => true,
						'prev_text' => 'Prev',
						'next_text' => 'Next',
						'type' => 'list'
						));
					?>
					
			<?php else : ?>
		
				<h2>Nothing found</h2>
		
			<?php endif; ?>
		</div>
		<div class="fivecol last">
			
			<section id="news-of-note">
				<div id="news-of-note-subscription">
					<p>To receive <span class="darkblue bold">News of Note</span> daily in your inbox, please subscribe</p>
					<?php gravity_form(42, false, false, false, '', false); ?>
				</div>
			</section>
		
			<p class="section-headline">wild<strong class="darkblue">art</strong> // <a href="<?php echo get_category_link(479); ?>" title="Wild Art Archives">archives</a></p>
			<section>
				<ul class="wild-art-section wildartarchive wildartsidebar">
				
				<?php $recent = new WP_Query( array( 
								'category_name'		=> 'wildart',
								'posts_per_page'			=> 2
								) );
				while ($recent->have_posts()) : $recent->the_post(); ?>
					<li>
						<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
						<h2><?php the_title(); ?></h2>
						<?php the_post_thumbnail( 'wildart-thumb' ); ?>	
						</a>
					</li>
					
				<?php endwhile; ?>
								
				<?php wp_reset_postdata(); ?>
				
				</ul>
				
			</section>
			
			
		</div>
	</div><!--ending row-->
</div><!-- /ending above the fold container-->

<?php get_footer(); ?>
