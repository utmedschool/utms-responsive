<?php include('header-new.php') ?>

<div class="container" id="logoSection">
			
	<div id="mainLogo" class="row noSlider">
	
		<section class="sevencol">
			<h1 id="pageTitle"><a href="<?php echo get_category_link( 3); ?>" title="News">News-WIP</a></h1>
		</section>
		
		<section class="fivecol last">
			<h2 id="utmsasSecondary"><a href="<?php echo site_url(); ?>" title="University of Texas Medical School at Houston" target="_parent"><img src="<?php bloginfo('template_directory'); ?>/img/logo/medschool-logo-large-current.png" alt="University of Texas Medical School at Houston" /></a></h2>
		</section>
		
	</div><!--end row-->
	
</div><!-- ending #logoSection-->
		
<?php include('includes/mission-nav.php'); ?>

<div class="container" id="mainContent">
	
	<div class="row">
	
		<section id="mainArticle" class="ninecol">
					
			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>	
				
				<article <?php post_class() ?> id="post-<?php the_ID(); ?>">
					
					<p><?php edit_post_link('Edit this entry',''); ?></p>
					
					<h1 class="single-headline"><?php the_title(); ?></h1>
					
					<p class="tags"><?php the_tags( '', '', '' ); ?></p>
					<!--<p><?php the_tags(); ?></p>-->
					
					<?php $scoop = get_post_meta($post->ID, 'scoop-link', TRUE); ?>
		       		
		       		<?php if($scoop != '') {?>
						<div class="scoopLink">
							<p><a href="<?php echo $scoop; ?>" title="Scoop">This article is also available in <img src="<?php bloginfo('template_directory'); ?>/img/logo/scoop-logo.png" alt="Scoop" class="scoopLogo" /></a></p>
						</div>
					<?php } ?>

		       		<div class="entry">

		       		<!--<?php
		                if(has_post_thumbnail()) { ?>
		                    <div class="entry-thumbnail">
		                        <?php the_post_thumbnail('single-featured-news');?>
		                    </div>
		            <?php } ?> -->
		            
		            <?php the_content();?>
		       		
		       		</div>
				
				</article>
				
				<?php comments_template(); ?>
			
		</section><!-- ending #contentDiv -->
		
		<section class="threecol last">
			<?php //get_sidebar(); ?>
			<div id="tags" class="news-tabs">
				   <ul class="news-tabs-nav tabs-clearfix">
				      <li><a href="#related-articles">Related Articles</a></li>
				      <li><a href="#all-tags">All Tags</a></li>
				   </ul>
				   <div id="related-articles" class="news-tabs-panel">
				      <div>
				      <?php
						$tags = wp_get_post_tags($post->ID);
						if ($tags) {
							$tag_ids = array();
							foreach($tags as $individual_tag) $tag_ids[] = $individual_tag->term_id;
						
							$args=array(
								'tag__in' => $tag_ids,
								'post__not_in' => array($post->ID),
								'showposts'=>10, // Number of related posts that will be shown.
								'caller_get_posts'=>1
							);
							$my_query = new wp_query($args);
							if( $my_query->have_posts() ) {
								echo '<ul>';
								while ($my_query->have_posts()) {
									$my_query->the_post();
								?>
									<li><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></li>
								<?php
								}
								echo '</ul>';
								}
							}
						?>
						<?php endwhile; endif; ?>
					  </div>
				   </div>
				   <div id="all-tags" class="news-tabs-panel">
				      <div class="tagcloud">
				      	<?php wp_tag_cloud('smallest=8&largest=16&orderby=count&order=DESC'); ?>
				   </div>
				</div>
			</div>
			<?php include (TEMPLATEPATH . '/includes/righticon-buttons.php'); ?>
		</section>
	
	</div>
	
</div><!--ending #mainContent-->

<?php get_footer(); ?>