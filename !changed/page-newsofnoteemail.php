<?php 
/*
Template Name: NewsofNote Email
*/
?>
<!doctype html>
<html>

<head>
	<title><?php echo the_title(); ?></title>
	<meta name="google-site-verification" content="TimdGzuLl1Yw4Dq45FMXkoWoVgJiZXFGs9itQCqhjXg">	
	<meta name="author" content="Office of Communications, Darla Brown, Director of Communications">
	<meta name="Copyright" content="Copyright UTHealth Med School 2012. All Rights Reserved.">
	<link rel="shortcut icon" href="<?php bloginfo('template_directory'); ?>/img/common/favicon.ico">		 
	<?php wp_head(); ?>
	
</head>

<div align="center">
  <table width="600" align="center" border="0" cellpadding="0" cellspacing="0" style="border:none; padding:10px 0 10px 0; width:600px;">
  <tr>
  	<td><p style="text-align:left;width:550px;font-family:Helvetica,Arial,sans-serif;font-size:12px;line-height:1.1em; color:#333;">Having trouble reading this email? <a href="https://med.uth.edu/news#news-of-note?utm_source=newsletter&utm_medium=email&utm_campaign=NewsofNoteDaily" style="color:#3589AE;text-decoration:underline;" title="Read this e-mail on our website">View it on the news section</a>.</td>
  </tr>
    <tr>
      <td><img src="https://med.uth.edu/wp-content/themes/utms-responsive/img/newsofnote/logo_NewsofNote.jpg" alt="News of Note" width="600" height="80" /></td>
    </tr>
    <tr>
      <td align="left" valign="top" style="background-color:#FFFFFF; padding:20px; padding-top:0; font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#333333;">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td align="left" valign="top" style="background-color:#FFFFFF; font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#333333;"><p><span style="font-size:20px"><strong style="font-size:22px;"><?php echo date('F d, Y'); ?></strong></span><br />
              Produced by the <a href="http://med.uth.tmc.edu/comm/" target="_blank" style="color:#1B708C;">Office of Communications</a></p>            </td>
            <td align="right" valign="top" style="background-color:#FFFFFF; font-family:Arial, Helvetica, sans-serif; font-size:10px; color:#333333;"><a href="http://med.uth.tmc.edu/" target="_blank"><img src="https://med.uth.edu/wp-content/themes/utms-responsive/img/newsofnote/NewsofNote-UTHealth-MS-logo.jpg" alt="The University of Texas Medical School at Houston" width="260" height="60" border="0" /></a></td>
          </tr>
        </table>
        <table width="100%" border="0" cellspacing="2" cellpadding="2" style="margin:10px 0;">
          <tr>
            <td align="left" valign="top" style="background-color:#FFFFFF; font-family:Arial, Helvetica, sans-serif; font-size:10px; padding:0;" width="50%"><strong>Medical School</strong>&nbsp;&nbsp;<a href="http://www.facebook.com/utmedschool" style="color:#454545; text-decoration:none"><img src="https://med.uth.edu/wp-content/themes/utms-responsive/img/scoop/icon-facebook.png" alt="Medical School's Facebook Fan Page" width="16" height="16" border="0" /> Facebook</a>&nbsp;&nbsp;<a href="http://twitter.com/utmedschool" style="color:#454545; text-decoration:none"><img src="https://med.uth.edu/wp-content/themes/utms-responsive/img/scoop/icon-twitter.png" alt="Medical School's Twitter Page" width="16" height="16" border="0" /> Twitter</a>&nbsp;&nbsp;<a href="http://www.youtube.com/user/utmedicalschool" style="color:#454545; text-decoration:none"><img src="https://med.uth.edu/wp-content/themes/utms-responsive/img/scoop/icon-youtube.png" alt="Medical School's YouTube Channel" width="16" height="16" border="0" /> YouTube</a></td>
            <td align="right" valign="top" style="background-color:#FFFFFF; font-family:Arial, Helvetica, sans-serif; font-size:10px; padding:0;" width="50%"><strong>Office of Communications</strong>&nbsp;&nbsp;<a href="http://www.facebook.com/utooc" style="color:#454545; text-decoration:none"><img src="https://med.uth.edu/wp-content/themes/utms-responsive/img/scoop/icon-facebook.png" alt="The OOC's Facebook Fan Page" width="16" height="16" border="0" /> Facebook</a>&nbsp;&nbsp;<a href="http://twitter.com/theooc" style="color:#454545; text-decoration:none"><img src="https://med.uth.edu/wp-content/themes/utms-responsive/img/scoop/icon-twitter.png" alt="The OOC's Twitter Page" width="16" height="16" border="0" /> Twitter</a></td>
          </tr>
        </table>
        <?php
        //$year = date('Y');
        //$week = date('W');
		//$day = date('d');
        
       // $recent = new WP_Query( 'year=' . $year . '&w=' . $week . '&day=' . $day. '&category_name=newsofnote&showposts=-1');while ($recent->have_posts()) : $recent->the_post(); ?>
        
<?php  //$recent = new WP_Query('w='. $week .'&category_name=newsofnote&showposts=-1'); while ($recent->have_posts()) : $recent->the_post(); ?>
		
<?php
	wp_reset_postdata();
	$today = getdate();
	$recent = new WP_Query( 'year=' . $today["year"] . '&monthnum=' . $today["mon"] . '&day=' . $today["mday"]. '&category_name=newsofnote&showposts=-1'); while ($recent->have_posts()) : $recent->the_post(); ?>

        
        <h2 style="color:#0E6180; font-weight:normal; font-size:16px; border-top:solid 1px #EEEEEE; padding-top:5px; text-transform:uppercase;"><?php the_title(); ?></h2>
		<p><?php the_excerpt(); ?></p>
		<?php $newsofnote = get_post_meta($post->ID, 'direct-external-link', TRUE); ?>
			<?php if($newsofnote != '') {?>
					<p style="font-size:10px;"><a href="<?php echo $newsofnote; ?>" title="External Link to <?php the_title(); ?>" style="color:#1B708C;">See full story</a></p>
			<?php } ?>
		
		<?php endwhile; ?>
		<?php wp_reset_postdata(); ?>		
		
		<h2 style="color:#0E6180; font-weight:normal; font-size:16px; border-top:solid 1px #EEEEEE; padding-top:5px; text-transform:uppercase;">&nbsp;</h2>
	  <p style="font-size:12px;">Some publications may require readers to register for free.</p></td>
    </tr>
    <tr>
      <td align="left" valign="bottom" style="background-color: #FFF7B0; padding: 20px; padding-top: 0px; font-family: Arial, Helvetica, sans-serif; font-size: 9px; color: #666666;"><br/>
      To be added to this mailing list, send an e-mail to <a href="mailto:scoop@uth.tmc.edu" style="color:#1B708C;">scoop@uth.tmc.edu</a> with the following command in the body of your e-mail message: <strong>subscribe ms-dean-news</strong> or <a href="http://med.uth.edu/news" style="color:#1B708C;">click here</a><br /><br />
      To be removed from this mailing list, send an e-mail to <a href="mailto:scoop@uth.tmc.edu" style="color:#1B708C;">scoop@uth.tmc.edu</a> with the following command in the body of your e-mail message: <strong>unsubscribe ms-dean-news</strong></td>
    </tr>
  </table>
</div>

	
	<?php wp_footer(); ?>
	
</body>
</html>