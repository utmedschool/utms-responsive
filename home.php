<?php get_header(); ?>

<div class="container" id="logoSection">
	<div id="mainLogo" class="row">
		<section class="twelvecol last">
			<h1><span class="sr-only">University of Texas Medical School at Houston</span><a href="<?php echo site_url(); ?>" title="University of Texas Medical School at Houston" target="_parent"><img src="<?php bloginfo('template_directory'); ?>/img/logo/medschool-logo-large-current.png" alt="University of Texas Medical School at Houston" /></a></h1>
		</section>
	</div><!--end row-->
</div><!-- ending #logoSection-->

<div id="rslider" class="container">
	<div class="row">
		<div id="flexcontainer" class="twelvecol last">
				<nav>
					<ul id="tabs-nav">
						<li><a href="<?php echo get_permalink(3603); ?>" title="Education">education</a></li>
						<li><a href="<?php echo get_permalink(3605); ?>" title="Patient Care">patient care</a></li>
						<li><a href="<?php echo get_permalink(3608); ?>" title="Research">research</a></li>
						<li><a href="<?php echo get_permalink(3610); ?>" title="Departments and Centers">departments</a></li>
			       </ul>
		       </nav>
			<div class="flexslider loadingif">
				<ul class="slides">
				
					<?php 
						$featureargs = array(
						'post_type' => 'featureimage',
						'posts_per_page' => 4,
						'feature_category'	=> 'homepage'
						);
						$featurequery = new WP_Query( $featureargs );
						if ( $featurequery->have_posts() ) : while ( $featurequery->have_posts() ) : $featurequery->the_post(); 
							
							$cf = get_post_custom($post->ID);
							$directlink = $cf['_info_link_url'][0];	
							$pagelink = $cf['_info_page_select'][0];	
							$postlink = $cf['_info_post_select'][0];
							
							if (!empty($directlink)){
								$finalurl = $directlink;
							} elseif ( (empty($directlink)) && (!empty($pagelink)) ) {
								$finalurl = get_permalink($pagelink);
							} elseif ( (empty($directlink)) && (empty($pagelink)) &&  (!empty($postlink)) ) {
								$finalurl = get_permalink($postlink);
							} else {
								$finalurl = '#';
							}
					?>
							<li><a href="<?php echo $finalurl; ?>"><?php the_post_thumbnail(); ?></a></li>
					<?php endwhile; else: ?>
							<li><a href="https://www.facebook.com/utmedschool" title="UTHealth Medical School"><img src="<?php bloginfo('template_directory');?>/img/slider-images/tmc-msb.jpg" alt="Texas Medical Center - UTHealth Medical School"></a></li>
					<?php endif;
						wp_reset_query();
					?>
					</ul>
			</div><!--ends flexslider-->
		</div><!--ends flexcontainer-->
	</div><!--ends .row-->
</div><!--ends rslider.container-->

<div class="container" id="mainContent">
	<div class="row">

	<section class="threecol">

		<!--<h2>links &amp; resources</h2>-->
		<nav>
		<?php
		wp_nav_menu(array(
			'menu' => 'links-and-resources',
			'container' => 'ul',
			'menu_id' => 'linksResources',
			'menu_class' => 'menu widget'
		)); ?>
		</nav>

		<h2>resources for</h2>
		<nav>
		<?php
		wp_nav_menu(array(
			'menu' => 'links for',
			'container' => 'ul',
			'menu_id' => 'linksFor',
			'menu_class' => 'menu widget'
		)); ?>
		</nav>

		
	</section>

	<section class="sixcol">

		<h2>headlines // <a href="<?php echo get_category_link( 3); ?>" title="Medical School News">more news</a></h2>

		<article id="primaryNews" class="homeNews">

    <?php /*<h4><a href="https://vimeo.com/136719343">See all the action in the 2015 Medical School Retreat</a></h4>
    <div class="video-frame"><iframe src="https://player.vimeo.com/video/136719343?autoplay=0&loop=0" frameborder="0" width="547" height="303" allowfullscreen=""></iframe></div>
    <p><hr class="news-separator"></p> */ ?>

			<?php $recent = query_posts('category_name=homepage-primary&showposts=1'); while (have_posts()) : the_post(); ?>
				<div class="newsImg"><?php the_post_thumbnail( 'homenews-main' ); ?></div>
				<h4><a href="<?php the_permalink();?>" title="<?php the_title();?>"><?php the_title(); ?></a></h4>
				<?php the_excerpt(); ?>
				<p><a href="<?php the_permalink();?>" title="read more about <?php the_title();?>">read more &raquo;</a></p>
			<?php endwhile; ?>

			<?php wp_reset_query(); ?>

		</article>

		<div id="separatorid" class="white45"><hr class="news-separator"></div>

		<article class="secondaryNews homeNews">
			<?php $recent = query_posts('category_name=homepage-secondary&showposts=1'); while (have_posts()) : the_post(); ?>
			<div class="newsImg"><?php the_post_thumbnail( 'homenews-second' ); ?></div>
			<h4><a href="<?php the_permalink();?>" title="<?php the_title();?>"><?php the_title(); ?></a></h4>
			<?php the_excerpt(); ?>
			<p><a href="<?php the_permalink();?>" title="read more about <?php the_title();?>">read more &raquo;</a></p>
			<?php endwhile; ?>
			<?php wp_reset_query(); ?>

		</article>

		<div id="separatorid2" class="white45"><hr class="news-separator"></div>

		<article class="secondaryNews homeNews">
			<?php $recent = query_posts('category_name=homepage-secondary&showposts=1&offset=1'); while (have_posts()) : the_post(); ?>
			<div class="newsImg"><?php the_post_thumbnail( 'homenews-second' ); ?></div>
			<h4><a href="<?php the_permalink();?>" title="<?php the_title();?>"><?php the_title(); ?></a></h4>
			<?php the_excerpt(); ?>
			<p><a href="<?php the_permalink();?>" title="read more about <?php the_title();?>">read more &raquo;</a></p>
			<?php endwhile; ?>
			<?php wp_reset_query(); ?>

		</article>

	</section>

	<section class="threecol last" id="rightDiv">

		<div id="utpPromo" class="leftBoxes">
			<a href="http://www.utphysicians.com" title="UT Physicians Website" target="_blank"><img src="<?php bloginfo(template_directory); ?>/img/logo/utphysicians-large.png" alt="UT Physicians for the entire family" /></a>
			<p>Dedicated to patient quality and clinical effectiveness, <a href="http://www.utphysicians.com" title="UT Physicians Website">UT Physicians</a> is the clinical practice of the UTHealth Medical School. With more than <span class="bold">1,000 clinicians certified in more than 80 specialties,</span> our doctors are ready to care for you and your family. Call <span class="bigger bold orange">1.888.488.3627</span> for an appointment.</p>
		</div>

		<h2>connect with us</h2>
		
		<div id="facebookPromo" class="white45 socialset">
			<h2 style="border-bottom:none;"><a href="https://www.facebook.com/utmedschool" title="The University of Texas Medical School at Houston on Facebook" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/home/facebook.png" alt="UTMedSchool" class="social-avatar">
			The University of Texas Medical School at Houston <small>on Facebook</small></a></h2>
		</div>
		
		<div id="twitterPromo" class="white45 socialset">
			<h2 style="border-bottom:none;"><a href="https://www.twitter.com/utmedschool" target="_blank" title="The University of Texas Medical School at Houston on Twitter"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/home/twitter.png" alt="UTMedSchool" class="social-avatar">
			Follow @UTMedSchool <small>on Twitter</small></a></h2>
		</div>
		
		<div id="instagramPromo" class="white45 socialset">
			<h2 style="border-bottom:none;"><a href="http://instagram.com/utmedschool" target="_blank" title="The University of Texas Medical School at Houston on Instagram"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/home/instagram.png" alt="UTMedSchool" class="social-avatar">
			Follow @UTMedSchool <small>on Instagram</small></a></h2>
		</div>
		
		
		
	</section><!-- ending #utpPromo and Twitter Feed -->

	</div>

	</div><!--ending #mainContent-->

<div class="container" id="secondaryContent">

	<div class="row">

	<section class="sevencol" id="eventsSection">

		<h2>upcoming events</h2>

		<ul class="eventListing sixcol">

		<?php

		global $post;
		$all_events = tribe_get_events(array(
					'eventDisplay'=>'upcoming',
					'posts_per_page'=>3,
				));

			foreach($all_events as $post) { setup_postdata($post); ?>

		<li>
		<a href="<?php the_permalink(); ?>" id="post-<?php the_ID(); ?>">
		<div class="eventDate">
			<span class="month"><?php echo tribe_get_start_date($post->ID, false, 'M'); ?></span>
			<span class="day"><?php echo tribe_get_start_date($post->ID, false, 'j'); ?></span>
		</div>
		<div class="eventDetails">
		 <?php
		 $themeta = get_post_meta($post->ID, 'event-series', TRUE);
		 if ($themeta != '') { ?>
			<span class="event-series"><?php echo tribe_get_event_meta($post->ID, $meta = 'event-series'); ?>: </span>
		<?php } ?>
				<?php if (tribe_get_organizer()) { ?>
				<span class="event-organizer"><?php echo tribe_get_organizer(); ?> presents</span>
				<?php } ?>
		        <span class="event-title"><?php the_title(); ?></span>
		        	<?php if(tribe_get_venue()) : ?>
					<span class="event-venue">
						<?php echo tribe_get_venue(get_the_ID()); ?>
					, </span>
					<?php endif; ?>
				<span class="event-date">
				<?php if( !tribe_get_all_day() ) { ?>
					<?php echo tribe_get_start_date($post->ID, true, ' '); ?> <?php echo tribe_get_end_date( $post->ID, true, '–' );?><br /><?php echo tribe_get_start_date($post->ID, false, 'M j, Y'); ?>
				<?php } else { ?>
					All Day Event
				<?php } ?>
				</span>
		        <!--<span class="event-content"><?php echo get_the_content(); ?></span>-->
		 </div></a>
		    </li>
		<?php } //endforeach ?>
		</ul>
		<?php wp_reset_query(); ?>


		<div class="sixcol last">

		<ul class="eventListing">

		<?php
		global $post;
		$all_events = tribe_get_events(array(
					'eventDisplay'=>'upcoming',
					'posts_per_page'=>2,
					'offset' => 3
				));
		foreach($all_events as $post) { setup_postdata($post); ?>
		<li>
		<a href="<?php the_permalink(); ?>" id="post-<?php the_ID(); ?>">
		<div class="eventDate">
			<span class="month"><?php echo tribe_get_start_date($post->ID, false, 'M'); ?></span>
			<span class="day"><?php echo tribe_get_start_date($post->ID, false, 'j'); ?></span>
		</div>
		<div class="eventDetails">
		 <?php
		 $themeta = get_post_meta($post->ID, 'event-series', TRUE);
		 if ($themeta != '') { ?>
			<span class="event-series"><?php echo tribe_get_event_meta($post->ID, $meta = 'event-series'); ?>: </span>
		<?php } ?>
				<?php if (tribe_get_organizer()) { ?>
				<span class="event-organizer"><?php echo tribe_get_organizer(); ?> presents</span>
				<?php } ?>
		        <span class="event-title"><?php the_title(); ?></span>
		        	<?php if(tribe_get_venue()) : ?>
					<span class="event-venue">
						<?php echo tribe_get_venue(get_the_ID()); ?>
					, </span>
					<?php endif; ?>
				<span class="event-date">
				<?php if( !tribe_get_all_day() ) { ?>
					<?php echo tribe_get_start_date($post->ID, true, ' '); ?> <?php echo tribe_get_end_date( $post->ID, true, '–' );?><br /><?php echo tribe_get_start_date($post->ID, false, 'M j, Y'); ?>
				<?php } else { ?>
					All Day Event
				<?php } ?>
				</span>
		        <!--<span class="event-content"><?php echo get_the_content(); ?></span>-->
		 </div></a>
		    </li>
		<?php } //endforeach ?>

		</ul>
		<?php wp_reset_query(); ?>

		<?php /*<p class="simple-events-link"><a href="<?php echo tribe_get_events_link(); ?>" title="View the Events Calendar">View the events calendar</a></p>
			<p class="css3button" style="margin-bottom:20px;">
				<a href="<?php echo get_permalink(2994); ?>"><img src="<?php bloginfo('template_directory'); ?>/img/icons/calendar.png" alt="calendar" class="calendarinhomeButton" /><span class="calendarTxt">Submit an event to <br/>the calendar</span></a></p>*/ ?>

				<p class="css3button" style="margin-top:30px; margin-bottom:30px;"><a href="<?php echo tribe_get_events_link(); ?>" title="View the Events Calendar"><img src="<?php bloginfo('template_directory'); ?>/img/icons/calendar.png" alt="calendar" class="calendarinhomeButton" /><span class="calendarTxt">View the events calendar</span></a></p>
			<p class="simple-events-link">
				<a href="<?php echo get_permalink(2994); ?>" title="Submit an event to the calendar">Submit an event to the calendar</a></p>
		</div>

	</section>

	<section class="twocol" id="downloadSection">
	<h2>annual report</h2>
		<ul>
			<li>
				<a href="https://med.uth.edu/annualreport/" title="Year In ReviewWebsite">
					<img src="http://med.uth.edu/yearinreview/files/2015/09/cover.jpg" alt="Year In Review">
				</a>
				<br />
				<a class="downloadSectionTxt" title="Year In Review Website" href="https://med.uth.edu/yearinreview/">Year in Review<br><small>Learn more</small></a>
				</li>
		</ul>
	<h2>factbook</h2>
		<ul>
			<li>

				<a href="https://med.uth.edu/documents/2014/10/medical-school-factbook.pdf" title="Fact Book"><img src="<?php bloginfo('template_directory'); ?>/img/icons/factbook.png" alt="Fact Book">Download <small>(6.2MB)</small></a><br />
				or<br />
				<a href="mailto:ms.communications@uth.tmc.edu?Subject=Factbook" title="Email a request for Factbook" style="margin:0; padding:0 !important;">Request via email</a>
			</li>
		</ul>

	</section>

	<section class="threecol last">

		<?php include (TEMPLATEPATH . '/includes/righticon-buttons.php'); ?>

	</section>

	</div>
</div><!--ending #secondaryContent-->



<?php get_footer(); ?>
